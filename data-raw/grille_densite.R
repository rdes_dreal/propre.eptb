## code pour préparer la table `gille_densite`

grille_densite_7_niveaux_2024 <- readxl::read_excel("data-raw/extdata/grille_densite_7_niveaux_2024.xlsx", skip = 4)

grille_densite <- grille_densite_7_niveaux_2024 %>%
  dplyr::select(CODGEO, DENS) %>%
  dplyr::mutate(
    densite = dplyr::case_when(
      DENS == 1 ~ "A",
      DENS == 2 ~ "B",
      DENS == 3 ~ "B",
      DENS == 4 ~ "B",
      DENS == 5 ~ "C",
      DENS == 6 ~ "C",
      DENS == 7 ~ "C"
    ),
    libelle_densite = dplyr::case_when(
      densite == "A" ~ "Communes densément peuplées",
      densite == "B" ~ "Communes de densité intermédiaire",
      densite == "C" ~ "Communes rurales"
    )
  ) %>%
  dplyr::select(-DENS)

usethis::use_data(grille_densite, overwrite = TRUE)

remotes::install_gitlab('dreal-pdl/csd/utilitaires.ju', host = "gitlab-forge.din.developpement-durable.gouv.fr")

utilitaires.ju::use_data_doc(
  "grille_densite",
  description = "Table de densité de population des communes selon 3 catégories",
  source = "DREAL Pays de la Loire")



