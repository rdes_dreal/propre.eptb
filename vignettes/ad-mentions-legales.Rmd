---
title: "ad- Mentions légales"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{ad- Mentions légales}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup}
library(propre.eptb)
```

La fonction `creer_verbatim_mentions_legales()` sert à générer automatiquement les mentions légales.

Elle renseigne automatiquement les différents blocs du chapitre 'Mentions légales' de la publication, comme la date de publication, les coordonnées du service... 

En dehors de la date de publication, les paramètres sont renseignés automatiquement à partir des données fournies par les DREAL/DEAL (service, adresse, téléphone ...) par l'intermédiaire du package `{propre.datareg}`.


## Apercu

Voici un aperçu du résultat de son exécution pour la région Auvergne-Rhône-Alpes.

```{r verbatim_mentions_legales, eval = TRUE}
creer_verbatim_mentions_legales(nom_reg = "Auvergne-Rhône-Alpes", date_publication = "2024-10-29")
```

On accède au différents paragraphes proposés à l'aide des crochets ou du $, par exemple :   

```{r verbatim_mentions_legales dollar, eval = TRUE}
creer_verbatim_mentions_legales()$code_source
```

```{r verbatim_mentions_legales crochet, eval = TRUE}
creer_verbatim_mentions_legales(nom_reg = "Auvergne-Rhône-Alpes")[["direction_publication"]]
```


## Paramétrage de la date de publication 

Par défaut, la date de la publication correspond à la date de compilation, renvoyée par la fonction `lubridate::today()`.  
Si l'argument `date_publication` n'est pas modifié, on obtient donc la date du jour :     

```{r verbatim_mentions_legales date, eval = TRUE}
creer_verbatim_mentions_legales()[["date"]]
```

Pour tenir compte de la date de levée de l'embargo, il est possible de modifier ce comportement par défaut, en renseignant l'argument `date_publication`, et ce en respectant le format JJ/MM/AAAA.

```{r verbatim_mentions_legales date _perso, eval = TRUE}
creer_verbatim_mentions_legales(date_publication = "04/11/2024")[["date"]]
```

