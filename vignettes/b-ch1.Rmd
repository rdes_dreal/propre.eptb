---
title: "b- Chapitre 1"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{b- Chapitre 1}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup}
library(propre.eptb)
indicateurs_eptb <- lire_eptb_exemple()
```

# creer_verbatim_1_1

La fonction creer_verbatim_1_1() produit les commentaires automatiques du chapitre 1.

```{r creer_verbatim11, eval=FALSE, message=FALSE, warning=FALSE}
creer_verbatim_1_1(data = indicateurs_eptb, annee = 2023)
```

 `r creer_verbatim_1_1(data = indicateurs_eptb, annee = 2023)`

# creer_graphe_1_1

La fonction `creer_graphe_1_1()` produit l'histogramme de la contribution de chaque région au nombre total de projets.

```{r graphe11, message=FALSE, warning=FALSE}
creer_graphe_1_1(
  data = indicateurs_eptb, 
  annee = 2023, 
  note_de_lecture = ""
  )[["viz"]]
```

# creer_tableau_1_1

La fonction `creer_tableau_1_1()` permet la création du tableau comportant :

-   en colonnes : les variables
    -   le nombre de projets à l'année n
    -   la part dans le total régional
    -   l'évolution entre n et n-1
    -   l'évolution entre n et n-5
-   en lignes : les niveaux géographiques ou catégoriels
    -   les départements de la région
    -   les catégories de densité des communes aggrégées à l'échelle régionale
    -   la région en tant que zone de référence
    -   la zone de comparaison, France métropolitaine ou Département et Régions d'Outre-Mer

```{r tableau11, message=FALSE, warning=FALSE}
creer_tableau_1_1(
  data = indicateurs_eptb,
  annee = 2023,
  titre = "Evolution des projets de construction de maisons individuelles",
  note_de_lecture = "",
  add_scroll = FALSE
  )[["viz"]]
```

# creer_carte_1_1

La fonction `creer_carte_1_1` permet la création de la carte régionale interactive par EPCI des projets de construction de maisons individuelles.

```{r carte11, message=FALSE, warning=FALSE}
creer_carte_1_1(
  data = indicateurs_eptb,
  annee = 2023,
  titre = "Projets de construction de maisons individuelles par EPCI",
  note_de_lecture = ""
  )[["viz"]]
```



