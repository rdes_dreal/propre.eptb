---
title: "aa- Prise en main"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{aa- Prise en main}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup}
library(propre.eptb)
indicateurs_eptb <- lire_eptb_exemple()
```



# Installation

## Chargement du package

Le package {propre.eptb} fait appel à des données soumises au secret statistique pour la préparation des données. Ainsi, le package ne contient pas la table des indicateurs servant à la confection de la publication. Cette table, comme les données brutes de chaque millésime est uniquement disponible sur un espace sécurisé, le Serveur Rstudio. Le package ne fonctionne actuellement qu’à partir de ce serveur. Pour l’utiliser, il suffit de le charger (:warning: il faut être habilité à accéder au serveur et à la source EPTB) :

```{r eval=FALSE}

library("propre.eptb")
```

## Packages connexes

`{propre.eptb}` s'appuie sur différents packages extérieurs. **Ces packages ont été déjà installés** sur le serveur, il n'y a donc aucune manipulation à faire.

Les différents packages appelés par `{propre.eptb}` sont :

-   `{COGiter}`, pour les calculs sur la carte des territoires (gestion de fusion de communes, ...) ;

-   `{mapfactory}`, pour la mise en page des cartes ;

-   `{gouvdown}`, pour la gestion des thèmes ggplot, des palettes et du template bookdown conformes à la marque État ;

-   `{gouvdown.fonts}`, pour intégrer les polices de la marque État ;

-   `{propre.datareg}`, pour disposer dans la publication des informations régionales, type mentions légales, collectées via un tableur collaboratif.

Leur mise à jour a été demandée par l'équipe projet en même temps que celle du package de publication. 



# Création du projet de publication {#creerprojet}

Après avoir chargé le package `{propre.eptb}`, l'utilisateur crée son projet de publication en sélectionnant :\
*New projet \> New directory \> Projet type : Publication EPTB*

Une interface de sélection des paramètres s'ouvre alors :

![selection_parametres](img/ui_param.png)

L'utilisateur y définit son choix de répertoire de travail (`path`), le millésime des données EPTB pour lesquelles il souhaite réaliser une publication et la région observée, avant de lancer la création du projet.

Une fois les paramètres validés, RStudio créé un répertoire de travail contenant le squelette de la publication et ouvre le projet.

## Premier aperçu du projet bookdown {#premierapercu}

La publication se présente sous forme d'un [template bookdown](https://thinkr.fr/rediger-avec-bookdown-pourquoi-comment/), comprenant **4 chapitres thématisés et un chapitre introductif (chapô, édito).**

Chacun d'entre eux correspond à un fichier rmarkdown, peuplé des fonctions de création des illustrations et du commentaire automatisé. Ce sont ces fichiers que vous pouvez modifier pour personnaliser votre publication.

Les paramètres principaux figurent dans le fichier `index.Rmd`.  

Pour obtenir un premier aperçu de la publication HTML, il faut compiler le bookdown.

Cela se fait de trois manières différentes :

1.  Soit en ligne de commande, depuis la console :

```{r eval=FALSE}
rmarkdown::render_site(encoding = 'UTF-8')
```

2.  Soit en "clic-bouton" :
    -   depuis le **menu Addins** :\
        ![previsulation](img/addins_previewbook.png)

    -   depuis le panneau **Build** :\
    *Build / Build Website*\
    ![build_website](img/build_book.png)


Le panneau **Build** apparaît par exemple dès lors qu'un des fichiers Rmd est ouvert.

  
Le projet de publication est prêt une fois que les pourcentages de compilation sont arrivés à 100%.  

**Attention** : Pendant la compilation, vous verrez que certaines fonctions peuvent être mentionnées comme ayant des valeurs manquantes : cela correspond aux données qui ont été maquées pour tenir compte du secret statistique et/ou diffuser des données robustes.  


[1] "Attention : le verbatim 3_11 contient des NA."   
  
[1] "Attention : le verbatim 3_12 contient des NA."   

![publication](img/browser.png)

## Sauvegarde de la première publication HTML {#sauvegardeHTML}

Après avoir pris connaissance du contenu standard de la publication, on peut [le compléter et le personnaliser](#personnalisation).

Il est cependant conseillé de sauvegarder cette première sortie.

Les fichiers html produits ont été enregistrés dans le répertoire *\_book* à la racine du projet. Il convient alors de renommer ce répertoire, par exemple en *\_book_0* pour éviter de l'écraser lors d'une prochaine manipulation et ainsi pouvoir s'y référer ultérieurement.

![sauvegarde_version_publication](img/save_book.png)

# Personnalisation de la publication {#personnalisation}

Le principe même de propre.eptb est d'obtenir des publications régionales avec la même structure et facilement comparables d'une région à l'autre. Le contenu standard de la publication peut néanmoins être complété et personnalisé.

Celui-ci est réparti dans différents chapitres, correspondant chacun à un fichier .Rmd :

-   index.Rmd,
-   00-chapo_edito.Rmd,
-   01-ch1.Rmd,
-   02-ch2.Rmd,
-   03-ch3.Rmd,
-   04-ch4.Rmd,
-   05-methodo.Rmd,
-   06-mentions_legales.Rmd.

Le fichier index régit l'ensemble de la publication et n'a pas vocation à être modifié sauf dans certain cas qui seront précisés lors des explications des différentes possibilités de personnalisation. Le reste des chapitres, en revanche, peut être enrichi par de nouveaux commentaires. En particulier, le premier chapitre introductif `00-chapo_edito.Rmd` doit être complété.

Chaque chapitre commence par un titre (matérialisé par une phrase commençant par un \#) et un sous-titre (matérialisé par \##).

Les textes sont créés par des fonctions commençant par `creer_verbatim_` et insérés directement dans le corps du chapitre ([code Inline](#texteperso)).

Les illustrations quant à elles sont générées par des fonctions (`creer_carte_`, `creer_graphe_`, `creer_tableau_`) insérées dans des "chunks" (commençant par ```` ```{r, ...}``` ````) qui exécutent le code R.

## Visualisation des changements et sauvegarde de la publication HTML

Pour que chaque modification soit prise en compte et puisse être visualisée, il est indispensable d'enregistrer chaque chapitre modifié (c'est-à-dire chaque fichier .Rmd modifié) et de relancer la compilation du bookdown (voir § [« Premier aperçu du bookdown »](#premierapercu)).

Chaque version ainsi générée peut facilement être enregistrée de la même manière que pour la v0 (voir § [« Sauvegarde de la première publication HTML »](#sauvegardeHTML)).


### Ajout de textes à la publication {#texteperso}

Chaque chapitre peut être complété avec un paragraphe qui précise le contexte, qui met en exergue les spécificités de la région ou encore qui commente une illustration (graphique, carte, tableau).

Pour cela, il convient de repérer le verbatim ou le "chunk" générant l'illustration en parcourant les fichiers .Rmd ou les vignettes d'explication des fonctions du package et insérer le paragraphe au-dessus ou en-dessous.

Pour utiliser des chiffres dans le commentaire, il est vivement conseillé d'utiliser la fonctionnalité de code inline de Rmarkdown pour éviter toute saisie de chiffres susceptibles d'évoluer en cas de mise à jour des données.

Du [code inline](https://rmarkdown.rstudio.com/lesson-4.html) est une portion de code qui sera exécutée et dont le résultat sera incrusté au milieu du texte. Il s'insère à l'intérieur de 2 back ticks avec la syntaxe : ![](img/code_inline.png).

### Ecrire en Rmarkdown

Pour ajouter du texte, le format de rédaction suit la syntaxe ***markdown***.

C'est-à-dire que le texte est saisi comme dans n'importe quel éditeur de texte, mais avec une syntaxe de formatage légère propre au markdown.

Par exemple :

-   `# Un dièse pour un titre de niveau 1`\
-   `## Deux dièses pour un titre de niveau 2`\
-   `### Trois dièses pour un titre de niveau 3`\
-   `Une ligne vide, puis une ligne qui commence par le signe moins "-" pour démarrer une liste comme celle que vous lisez actuellement`\
-   `**Entourer son texte avec deux doubles étoiles pour qu'il apparaisse en gras**`\
-   `_Entourer son texte avec deux underscores ("tiret du 8") pour qu'il apparaisse en italique_`\
-   `[Afficher un lien vers internet avec des crochets pour le texte et des parenthèses pour l'url](https://url-ici.com)`

Pour commencer un nouveau paragraphe, saisir deux espaces à la fin de la ligne précédente et aller à la ligne ou aller à la ligne deux fois.

Plus d'exemples sur [wikipedia](https://fr.wikipedia.org/wiki/Markdown#Quelques_exemples) ou sur [fun-mooc](https://www.fun-mooc.fr/c4x/UPSUD/42001S02/asset/RMarkdown.html).

## Suppression de verbatim, tableau et/ou graphe si nécessaire

Dans le cas de cette publication, il peut être nécessaire de supprimer des verbatims, des tableaux et/ou des graphiques qui contiendraient trop de valeurs non diffusées pour être commentées. 

Pour supprimer une partie, il convient dans un premier temps de repérer la fonction qui la génère, ensuite, il y a deux solutions :

1. ajouter un \# devant la fonction pour la mettre en commentaire. Cela permet de tout de même conserver la structure initiale : 
  
  Exemple pour un verbatim : 
    
```{r eval=FALSE}
r #creer_verbatim_1_1(data = indicateurs_eptb, annee = annee)
```
  Exemple pour une fonction d'illustration, il ne faut pas afficher le visuel dans la publication ni dans le fichier Excel de sortie. 

```{r eval=FALSE}
{r graphe11, message=FALSE, warning=FALSE}

 graphe_1_1 <- creer_graphe_1_1(
   data = indicateurs_eptb,
   annee = annee,
   note_de_lecture = ""
   )
# exports_xls <- incrementer_export_xls(graphe_1_1, list_xls = exports_xls)
# afficher_visuel(graphe_1_1)

```

Il est possible de le faire également devant toute fonction exécutée en R comme les sous-titres par exemple.

2. supprimer directement la fonction qui génère le paragraphe, mais pour le remettre ultérieurement, il faudra réécrire le code ou le texte.

## Suppression des sous-titres de la table des matières

La table des matières reprend tous les titres et sous-titres de la publication et peut ainsi paraître alourdie. Il est possible de retirer les sous-titres sans pour autant les enlever du corps du texte. Pour cela, il suffit d'ajouter {.unlisted} à la fin du sous-titre (ou titre) que l'on souhaite retirer.

## Suppression d'un chapitre  

Si vous souhaitez enlever un chapitre à votre publication, il suffit de supprimer (sauvegarder le plutôt, on peut changer d'avis) le chapitre correspondant. Par exemple, si vous décidez de ne pas faire apparaître le chapitre 2, il faut enlever de votre projet le fichier "02-ch2.Rmd". Lors de la compilation du book, la numérotation des chapitres sera recalculée. 


## Paramétrage des illustrations {#paramillust}

Toutes les fonctions d'illustration proposent un rendu paramétrable par l'utilisateur.

### Titres et notes de lectures {#titreillust}

Les fonctions des familles `creer_tableau`, `creer_graphe` et `creer_carte` proposent deux arguments, `titre` et `note_de_lecture`, qui vous permettent d'adapter ou de compléter les formulations par défaut.

Pour insérer un saut de ligne à votre titre de tableau, ajoutez la chaîne de caractère `<br>` à l'endroit souhaité pour le retour à la ligne.\
Pour insérer un saut de ligne à votre titre de carte ou graphique, ajoutez la chaîne de caractère `\n` à l'endroit souhaité pour le retour à la ligne.

Utilisez le paramètre `note_de_lecture` si vous souhaitez ajouter une note de lecture ou autre mention en bas de l'illustration.

Voici un exemple d'adaptation du titre et d'ajout d'une note de lecture.

```{r exemple titre note lecture}
creer_tableau_1_1(
  data = indicateurs_eptb,
  annee = 2023,
  titre = "Mon titre spécifique <br> sur 2 lignes",
  note_de_lecture = "Une note de lecture",
  add_scroll = FALSE
  )[["viz"]]
```

Ces paramètres sont sensibles aux questions d'encodages, veuillez utiliser les caractères ASCII et enregistrer vos fichiers Rmd avec l'encodage UTF-8.

### Numérotation des illustrations

Chaque illustration possède un titre par défaut mais comme vu précédemment (§[Titres et notes de lecture](#titreillust)), ce titre peut être modifié et une numérotation peut alors être ajoutée manuellement. Il sera alors possible d'y faire référence en fin de verbatim ou dans le texte ajouté (cf. § [Modification des textes de la publication](#texteperso)).

### Tableaux

En cas de liste de départements très longue et qui gêne la lecture, on peut ajouter une barre de défilement au tableau et ainsi le raccourcir. Pour cela, mettre le paramètre `add_scroll = TRUE` .

```{r exemple scroll tableau}
creer_tableau_1_1(
  indicateurs_eptb,
  annee = 2023,
  titre = "Mon titre spécifique <br> sur 2 lignes",
  note_de_lecture = "Une note de lecture",
  add_scroll = TRUE
  )[["viz"]]

```

# Trouver de l'aide

La documentation a fait l'objet d'une attention particulière de la part des auteurs. Le fonctionnement et l'utilisation des fonctions sont documentés dans l'aide du package et dans ses [vignettes](https://rdes_dreal.gitlab.io/propre.eptb/index.html).

L'utilisateur est invité à signaler les bugs et difficultés rencontrés ou à faire d'éventuelles demandes de nouvelle fonctionnalité via le système de gestion de tickets de gitlab dédié à `{propre.eptb}`, à l'adresse <https://gitlab.com/rdes_dreal/propre.eptb/-/issues>.


# Où diffuser les HTML en sortie ?

L'url du serveur de publication est : dreal.statistiques.developpement-durable.gouv.fr.   
Vous trouverez un pas à pas détaillé dans le wiki du projet [propre.rpls](https://gitlab.com/rdes_dreal/propre.rpls/-/wikis/Diffusion-des-publications)

# Comment produire une sortie PDF de la publication ?

La fonction expérimentale `creer_eptb_pdf_book()` crée une sortie pdf de la publication html, elle s'appuie pour cela sur le moteur d'impression pdf de chrome.exe.
L'argument _chemin_book_ permet de spécifier le répertoire d'un book html existant (par défaut "_book/").
Si cet argument désigne un nouveau répertoire, le bookdown y sera compilé en html avant sa conversion en pdf.
La sortie pdf sera créé dans le dossier de la publication html (par défaut "_book/").

```{r eval=FALSE}
creer_eptb_pdf_book(chemin_book="_book/", nom_pdf = "book_complet.pdf", scale = 0.9)
```

Plus d'information sur l'export pdf sur la [page d'aide de la fonction `creer_pdf_book()`](https://rdes_dreal.gitlab.io/propre.eptb/reference/creer_eptb_pdf_book.html). 

Dans le contexte du serveur RStudio des DREAL, le moteur d'impression pdf de chrome n'est pas encore opérationnel. 
Cette difficulté peut être soulignée auprès du gestionnaire du serveur pour appuyer la demande d'amélioration de la configuration de chrome.

La solution temporaire est d'exporter le dossier book de votre projet, au format zip, à l'aide de la roue crantée de votre panneau files : 

```{r, echo = FALSE, out.width="100%"}
knitr::include_graphics("img/export_zip_book.png")
```

Il faut cocher le dossier `_book`, puis cliquer sur la roue crantée, puis cliquer sur `Export...`
Après avoir enregistré le zip sur votre PC, puis de-zippé votre dossier, ouvrez votre RStudio pour exécuter la fonction `creer_eptb_pdf_book()` en adaptant le paramètre `chemin_book` avec l'adresse d'enregistrement de votre dossier dé-zippé :

```{r eval=FALSE}
creer_eptb_pdf_book(chemin_book="chemin/local/ou/est/de-zippee/ma_publi/_book/", nom_pdf = "book_complet.pdf")
```

Dons notre exemple, le fichier `book_complet.pdf` sera créé dans le dossier `chemin/local/ou/est/de-zippee/ma_publi/_book/`.

> **Important** : pour que le moteur d'impression de chrome puisse fonctionner, il est nécessaire de renseigner la variable d'environnement `NO_PROXY` dans votre .Renviron conformément aux [préconisations du PNE logiciels statistiques](https://numerique.metier.e2.rie.gouv.fr/offre-de-service-r-sas-a25995.html#Guide-pour-l-installation-de-R-et-RStudio-sur-votre-poste-de-nbsp) 



