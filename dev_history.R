# création du package ----------
# New project / New directory/create R package using devtools


# création d'un fichier dev_history.R à la racine du projet ---------
usethis::edit_file("dev_history.R")
# Ignorer le dossier dev/ dans Rbuildignore car il ne doit pas être pris en compte dans la création du package
usethis::use_build_ignore("dev_history.R")


# License du package (à revoir) ----------
usethis::use_gpl3_license()


# Activer git et relier avec le serveur distant -----------
# projet créé préalablement dans Gitlab, ce qui permet de déclarer l'adresse par la suite
usethis::use_git()
# indiquer au git du serveur CGDD qui on est, comme on l'avait fait sur son PC, une fois pour toutes
# git config --global prenom.nom "Prenom Nom"
# git config --global user.email  prenom.nom@developpement-durable.gouv.fr


# En préalable à la déclaration du projet distant à gitlab par un premier push (à compléter) -----
# git config --global http.proxy http://proxy-rie.ac.i2:8080;git config --global https.proxy http://proxy-rie.ac.i2:8080; git config --global credential.proxy http://proxy-rie.ac.i2:8080
# création de .git-credentials (quadrant droit bas / More / Show Hidden Files) à la racine de l'espace personnel

# usethis::use_git_remote(url = "git@gitlab.com:rdes_dreal/propre.eptb.git", overwrite = TRUE) # correspond à clé SSH : ne fonctionne pas
usethis::use_git_remote(url = "https://gitlab.com/rdes_dreal/propre.eptb.git", overwrite = TRUE)



# déclarer le projet distant à gitlab par un premier push ----------
# selon les indications de Gitlab:
# on écrit dans le terminal de RStudio :
# git add .
# git commit -m "Initial commit"
# git push --set-upstream origin master


# gérer les caractères de fin de ligne -------------
# partie 12.5.4.1 du guide propre
# (https://rdes_dreal.gitlab.io/publication_guide/dev-gerer-gitlab.html#soccuper-du-versionnement-avec-git)
# sur Windows, les caractères sont convertis au style Unix :
# on écrit dans le terminal de RStudio : git config --global core.autocrlf true
# création d'un fichier nommé .gitattributes (Files/New Blank File dans le quadrant droit bas)
# le fichier .gitattributes doit être commit avec les autres fichiers
# pour le contenu voir aussi le projet propre.rpls :
# https://gitlab.com/rdes_dreal/propre.rpls/-/blob/master/.gitattributes?ref_type=heads


# documenter le dépôt git avec un README -----------
usethis::use_readme_rmd()

# problème à partir de là :
# remotes::install_github("ThinkR-open/chameleon")
# installe le package {papillon} (?)
# chameleon::create_pkg_desc_file(to = "markdown")
papillon::create_pkg_desc_file(to = "markdown") # crée un fichier pkg_description.md temporaire ?
# [WARNING] This document format requires a nonempty <title> element.
# Defaulting to 'content.knit' as the title.
# To specify a title, use 'title' in metadata or --metadata title="...".
# File /tmp/Rtmpyp3HTm/pkg_description.md created
# • Modify '/tmp/Rtmpyp3HTm/pkg_description.md'
# You can now edit /tmp/Rtmpyp3HTm/pkg_description.md
papillon::create_pkg_desc_file(path = "README", to = "markdown")
# Erreur dans idesc_create_file(self, private, file) :
#   is_existing_file(file) n'est pas TRUE

# README.md créé par Knit manuel de README.Rmd


# documenter les modifications du projet avec les NEWS ----------
usethis::use_news_md()
# ne produit pas le résultat attendu !
# fichier NEWS.md corrigé à la main !


# initialiser l’intégration continue et la réécrire ----------
usethis::use_gitlab_ci()
usethis::use_build_ignore(".gitlab-ci.yml")

# le script est retravaillé

# déclarer les variables GITHUB_PAT et GITLAB_PAT dans les paramètres CI/CD :

# En préalable, il faut générer un nouveau token selon la procédure indiquée ici :
# https://docs.github.com/fr/authentication/keeping-your-account-and-data-secure/managing-your-personal-access-tokens#cr%C3%A9ation-[…]al-access-token
# En effet, un token sert à identifier un compte pour une seule machine (PC ou serveur).
# Il faut donc en générer autant que de machines qui exécutent le code qui en a besoin.
# Variables -> Etendre -> Ajouter une variable
# -> Clé pour GITHUB_PAT (le token nouvellement généré)

# lors de la phase build de l'intégration continue sur Gitlab :
# Error: R CMD check found WARNINGs :
# checking for executable files ... WARNING
# Found the following executable file:
#   ci/lib/Rttf2pt1/exec/ttf2pt1
# Source packages should not contain undeclared executable files.
# See section ‘Package structure’ in the ‘Writing R Extensions’ manual.
# Il faut alors ajouter une ligne à .Rbuildignore : ^ci/lib/Rttf2pt1/exec/ttf2pt1$
# comme exécutable indésirable qui ne doit pas être inclus dans le package {fg.propre.eptb}


# intégration du pipe dans le package {propre.eptb} ------
usethis::use_pipe() # ajoute le package {magrittr} à DESCRIPTION/imports pour pouvoir disposer du pipe %>%


# première data préparation -----
usethis::use_data_raw()
# cette commande crée 'data-raw/' et ajoute '^data-raw$' à '.Rbuildignore'
# elle crée aussi 'data-raw/DATASET.R' pour préparer les données
# les indicateurs sommables ont été calculés à l'échelle communale sans souci de secrétisation.
# ils donnent lieu à une table tab_result qui est versée dans  /nfs/data/partage-EPTB/propre.EPTB/


# création de la fonction de datapréparation pour la publication de l'utilisateur ----------
# le package {mapfactory} est mis à contribution pour construire la fonction dataprep()
usethis::use_r("dataprep")
usethis::use_test("dataprep")


# insérer un template de publication -----------
# créer un projet bookdown vide avec gouvdown dans RStudio :
# File/New Project/New Directory/Book Project using gouvdown
# en l'occurence, Directory Name : Book ; Create project as subdirectory of : C:/Users/prenom.nom/Documents/projets
# placer le projet de book dans : inst/rstudio/template/project/ressources après avoir créé le répertoire
dir.create("inst/rstudio/templates/project/ressources", recursive = TRUE)
# supprimer le fichiers book.Rproj, default.css et gouv_book.css


# création de skeleton.R -----
# contient les fonctions à mettre en place le projet de publication
usethis::use_r("skeleton")
# copie et adaptation du R/skelton.R de {propre.rpls}

# fonction propre.eptb_file : renvoie les adresses absolues des fichiers presents
# à adapter d'après propre.rpls_file
# dans le repertoire d'installation du package a partir de leur adresse relative
usethis::use_test("propre.eptb_file.R")

# fonction propre.eptb_skeleton : creation du repertoire du projet de l'utilisateur
# à adapter d'après propre.rpls_skeleton
# neutraliser la partie "Copier le dossier 'resources/extdata' vers le repertoire de travail utilisateur"
# si on a rien à y mettre, ce qui est le cas en l'occurrence
usethis::use_test("propre.eptb_skeleton.R")

usethis::use_vignette("zz-creation_du_projet_de_publication", "zz- Creation du projet de publication")
dir.create("vignettes/img", recursive = TRUE)
# création du répertoire vignettes/img pour y stocker les images des vignettes
# en l'occurrence la sélection des paramètres de la publication :
# (annee = 2022, nom_reg = "Pays de la Loire")

# création du fichier skeleton.dcf "à la main" : File/New File/Text File
# et enregistrer sous "inst/rstudio/template/project/skeleton.dcf"
# copie du contenu du fichier du même nom dans {propre.rpls} et adaptation
# copie de propre.rpls.png : à remplacer par une icone EPTB


# création de globals.R (à supprimer si possible) ---------
usethis::use_r("globals.R")


# création d'un jeu de données exemple -----
usethis::use_data_raw("indicateur_eptb_exemple")
# calcul du jeu de données exemple et dépôt dans inst : "inst/data-examples/indicateurs_rpls_exemple.csv"
# il va permettre de développer des fonctions de verbatim, graphe, tableau et carte


# création de la fonction lire_eptb_exemple -----
usethis::use_r("lire_eptb_exemple")
# déjà créée dans R/skeleton.R, la commande : propre.eptb::propre.eptb_file() (cf. insérer un template de publication)
usethis::use_test("lire_eptb_exemple")


# création de la grille de densité de population des communes en 3 catégories (COG 2023) ----
usethis::use_data_raw("gille_densite")


# palettes -----------
# gouvdown::gouv_palettes # pour montrer les palettes de gouvdown


# caption des illustrations
usethis::use_r("caption")
usethis::use_test("caption")


# chapitre 1 ----------
usethis::use_vignette("b-ch1","b- Chapitre 1")
usethis::use_r("creer_verbatim_1_1")
usethis::use_r("creer_graphe_1_1")
usethis::use_test("creer_graphe_1_1")
usethis::use_r("creer_tableau_1_1")
usethis::use_test("creer_tableau_1_1")
usethis::use_r("creer_carte_1_1")
usethis::use_test("creer_carte_1_1")


# chapitre 2 -----------
usethis::use_vignette("b-ch2","b- Chapitre 2")
usethis::use_r("creer_verbatim_2_1")
usethis::use_r("creer_verbatim_2_2")
usethis::use_r("creer_verbatim_2_3")
usethis::use_r("creer_tableau_2_1")
usethis::use_r("creer_verbatim_2_4")
usethis::use_r("creer_graphe_2_1")
usethis::use_r("creer_verbatim_2_5")
usethis::use_r("creer_graphe_2_2")


# chapitre 3 -------------
usethis::use_vignette("b-ch3","b- Chapitre 3")
usethis::use-r("creer_verbatim_3_1")
usethis::use_r("creer_verbatim_3_2")
usethis::use_r("creer_tableau_3_1")
usethis::use_test("creer_tableau_3_1")
usethis::use_r("creer_graphe_3_1")
usethis::use_test("creer_graphe_3_1")
usethis::use_r("creer_verbatim_3_3")
usethis::use_r("creer_verbatim_3_4")
usethis::use_r("creer_carte_3_1")
usethis::use_test("creer_carte_3_1")
usethis::use_r("creer_verbatim_3_5")
usethis::use_r("creer_graphe_3_2")
usethis::use_test("creer_graphe_3_2")
usethis::use_r("creer_verbatim_3_6")
usethis::use_r("creer_tableau_3_2")
usethis::use_test("creer_tableau_3_2")
usethis::use_r("creer_verbatim_3_7")
usethis::use_r("creer_verbatim_3_8")
usethis::use_r("creer_verbatim_3_9")
usethis::use_r("creer_verbatim_3_10")
usethis::use_r("creer_verbatim_3_11")
usethis::use_r("creer_verbatim_3_12")
usethis::use_r("creer_graphe_3_3")
usethis::use_test("creer_graphe_3_3")
usethis::use_r("creer_graphe_3_4")
usethis::use_test("creer_graphe_3_4")
usethis::use_r("creer_verbatim_3_13")


# chapitre 4 ----------
usethis::use_vignette("b-ch4","b- Chapitre 4")
usethis::use_r("creer_verbatim_4_1")
usethis::use_r("creer_graphe_4_1")
usethis::use_test("creer_graphe_4_1")
usethis::use_r("creer_carte_4_1")
usethis::use_test("creer_carte_4_1")
usethis::use_r("creer_verbatim_4_2")
usethis::use_r("creer_graphe_4_2")
usethis::use_test("creer_graphe_4_2")
usethis::use_r("creer_verbatim_4_3")
usethis::use_r("creer_verbatim_4_4")
usethis::use_r("creer_verbatim_4_5")
usethis::use_r("creer_verbatim_4_6")
usethis::use_r("creer_tableau_4_1")
usethis::use_test("creer_tableau_4_1")
usethis::use_r("creer_verbatim_4_7")
usethis::use_r("creer_verbatim_4_8")
usethis::use_r("creer_verbatim_4_9")
usethis::use_r("creer_tableau_4_2")
usethis::use_test("creer_tableau_4_2")


# vignettes hors chapitres ------------
# usethis::use_vignette("aa-prise-en-main", "aa- Prise en main")
# usethis::use_vignette("ab-preparation-donnees", "ab- Préparation des données")
# usethis::use_vignette("ac-caption", "ac- Caption des illustrations")
usethis::use_r("creer_verbatim_mentions_legales")
usethis::use_vignette("ad-mentions-legales", "ad- Mentions l\u00e9gales")


# fonctions l'exportation du fichier xls des données des illustrations ----------
usethis::use_r("utils")
usethis::use_vignette("ae-export-xls", "ae- Fonctionnement de l'export des données visualisées vers un tableur xls")


# fonction d'impression du book au format pdf
usethis::use_r("creer_eptb_pdf_book")


# produire un site du package ------------
usethis::use_pkgdown()
pkgdown::build_site() # après avoir actualisé le package {propre.eptb}

# le site apparait après avoir activé la commande
# en cliquant  sur docs/index.html une petite fenêtre propose d'ouvrir le site
# sur l'éditeur ou sur le web : choisir le web

# sur le serveur RStudio du SDES, la prévisualisation n'apparait pas normalement
# pour visualiser les pages du site :
# ouvrir le fichier propre.eptb/docs/index.html dans l’éditeur, à partir du panneau Files de RStudio
# puis cliquer sur le bouton preview pour visualiser le rendu


# création d’un projet utilisateur avec l’exemple de la fonction skeleton ----------
library(propre.eptb)
propre.eptb_skeleton(path = "/nfs/users/franck.gaspard/eptb_2022_bretagne", annee = "2022", nom_region = "53 Bretagne")
propre.eptb_skeleton(path = "/nfs/users/franck.gaspard/eptb_2022_guyane", annee = "2022", nom_region = "03 Guyane")
propre.eptb_skeleton(path = "/nfs/users/franck.gaspard/eptb_2022_aura", annee = "2022", nom_region = "84 Auvergne-Rhône-Alpes")
# La console lance la commande et affiche le message : [1] TRUE
# Ensuite faire “Nouveau projet/existing directory”
# et rechercher le nouveau répertoire eptb_2022_bretagne
# un fichier eptb_2022_bretagne.Rproj et un répertoire .Rproj.user sont créés automatiquement à la racine de ce nouveau projet
# aller dans l'onglet "Build" dans le quadrant droit haut et lancer "Build Website"
# si à la fin du calcul une "Popup Blocked" apparait, répondre "Cancel" et cliquer sur le fichier_book/index.html
# choisir "Open in Editor" et le fichier index.html s'ouvre dans le quadrant gauche haut
# cliquer alors sur "Preview"





# à faire régulièrement, avant de faire des commits et de pousser sur gitlab ---------
attachment::att_amend_desc()
# génère un répertoire dev à la racine contenant un fichier config_attachement.yaml, même avec use.config = FALSE
usethis::use_package("writexl", type = "suggests")
# devtools::install()
devtools::check()

devtools::load_all()





# le 07/06/2024 : Erreur : HTTP error 403 ----------

# devtools::install()
# Erreur : HTTP error 403.
# API rate limit exceeded for 185.24.184.194. (But here's the good news: Authenticated requests get a higher rate limit. Check out the documentation for more details.)
#
#   Rate limit remaining: 0/60
#   Rate limit reset at: 2024-06-07 13:38:45 UTC
#
#   To increase your GitHub API rate limit
#   - Use `usethis::create_github_token()` to create a Personal Access Token.
#   - Use `usethis::edit_r_environ()` and add the token as `GITHUB_PAT`.

# créer un .Renviron pour le serveur RStudio du SDES -----
usethis::edit_r_environ()
# et ajouter une ligne : GITHUB_PAT = nouveau_token
# le nouveau token a été créé sur son compte personnel sur Github
