---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```

# propre.eptb <img src="man/figures/logo_propre_eptb.png" align="right" height="139"/>

Le site web de présentation du package est accessible à ces adresses :

- pour la version master [https://rdes_dreal.gitlab.io/propre.eptb/index.html](https://rdes_dreal.gitlab.io/propre.eptb/index.html)

- pour la version en développement : [https://rdes_dreal.gitlab.io/propre.eptb/dev/index.html](https://rdes_dreal.gitlab.io/propre.eptb/dev/index.html) 

Le but de {propre.eptb} est de faciliter la production de publications régionales sur le prix des terrains à bâtir à partir de la source EPTB (enquête sur le prix des terrains à bâtir). Il contient un template bookdown paramétrable selon la région d'observation et les fonctions nécessaires à la création des commentaires et illustrations.

## Installation

Vous pouvez installer propre.eptb depuis gitlab avec :

``` r
remotes::install_gitlab("rdes_dreal/propre.eptb")
```

## Liens

-   [Le 4 pages de présentation de la démarche propre](http://dreal.statistiques.developpement-durable.gouv.fr/parc_social/2020/www/export_demarchePropre.pdf)
-   [Le rapport de présentation de la démarche propre](https://rdes_dreal.gitlab.io/propre/index.html)
-   [Le guide technique](https://rdes_dreal.gitlab.io/publication_guide/)

## Utilisation

-   Créez un nouveau projet Rstudio, dans un nouveau répertoire et sélectionner `Publication EPTB` comme type de projet ;

-   Sélectionner vos paramètres (millésime, région);

-   Complétez les champs auteurs et date dans index.Rmd ;

-   Lancez la compilation du bookdown ;

-   Intégrez vos commentaires et analyses ;-)

Plus de détail sur la prise en main du package au niveau de la vignette ["Prise en main"](https://rdes_dreal.gitlab.io/propre.eptb/articles/aa-prise-en-main.html)

## Licences

Tous les scripts sources du package sont disponibles sous licence [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html).

La [charte graphique de la marque d'État](https://www.gouvernement.fr/charte/charte-graphique-les-fondamentaux/la-typographie) est à usage exclusif des acteurs de la sphère étatique. En particulier, la typographie Marianne© est protégée par le droit d'auteur.

Toutes les informations sur enquête sur le prix des terrains à bâtir produites par ce package (données et textes) sont publiées sous [licence ouverte/open licence v2 (dite licence Etalab](https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf)) : quiconque est libre de réutiliser ces informations sous réserve, notamment, d'en mentionner la filiation.
