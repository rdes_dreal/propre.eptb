test_that("creer_graphe_3_2 fonctionne", {
  indicateurs_eptb <- lire_eptb_exemple()
  graph <- creer_graphe_3_2(
    data = indicateurs_eptb,
    annee = 2023,
    titre_prix = "Evolution du prix des maisons",
    titre_surface = "Evolution de la surface des maisons",
    note_de_lecture = ""
  )
  testthat::expect_s3_class(graph[["viz"]], c("girafe", "htmlwidget", "ggiraph" ))
  testthat::expect_s3_class(graph[["tab_xls"]], "data.frame")
  testthat::expect_s3_class(graph[["meta"]], "data.frame")
  testthat::expect_length(graph, 3)
})
