test_that("creer_tableau_3_2 fonctionne", {
  indicateurs_eptb <- lire_eptb_exemple()
  tableau <- creer_tableau_3_2(
    data = indicateurs_eptb,
    annee = 2023,
    titre = "Caractéristiques des maisons par type de maître d'oeuvre",
    add_scroll = FALSE
  )
  testthat::expect_equal(attr(tableau[["viz"]],'format'),'html')
  testthat::expect_s3_class(tableau[["tab_xls"]], "data.frame")
  testthat::expect_s3_class(tableau[["meta"]], "data.frame")
  testthat::expect_length(tableau, 3)
})

