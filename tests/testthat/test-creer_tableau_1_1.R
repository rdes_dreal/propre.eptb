test_that("creer_tableau_1_1 fonctionne", {
  indicateurs_eptb <- lire_eptb_exemple()
  tableau <- creer_tableau_1_1(
    data = indicateurs_eptb,
    annee = 2023,
    titre = "Evolution des projets de construction de maisons individuelles",
    add_scroll = FALSE,
    note_de_lecture = ""
    )
  testthat::expect_equal(attr(tableau[["viz"]],'format'),'html')
  testthat::expect_s3_class(tableau[["tab_xls"]], "data.frame")
  testthat::expect_s3_class(tableau[["meta"]], "data.frame")
  testthat::expect_length(tableau, 3)
})


