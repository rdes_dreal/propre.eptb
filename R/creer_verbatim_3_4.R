#'Création du verbatim 4 du chapitre 3
#'
#' @description Production du verbatim du chapitre 3.
#'
#' @param data La table d'indicateurs préparée par dataprep() selon les inputs de l'utilisateur.
#' @param annee Le millesime renseigné par l'utilisateur.
#'
#' @return Une chaîne de caractères comprenant les commentaires essentiels du chapitre 3.
#'
#' @importFrom dplyr select filter mutate across where pull arrange case_when
#' @importFrom glue glue
#' @importFrom mapfactory format_fr_nb
#' @importFrom propre.datareg datareg
#' @export
#'
#' @examples
#' indicateurs_eptb <- lire_eptb_exemple()
#' creer_verbatim_3_4(data = indicateurs_eptb, annee = 2023 )
#'
creer_verbatim_3_4 <- function(data, annee) {

  # Étape 1 : Filtrer les données pour les régions de métropole et la France
  data <- data %>%
    dplyr::select(
      "TypeZone":"Zone_de_comp","cout_prev_maison_moy_m2"
    ) %>%
    dplyr::filter(TypeZone %in% c("R\u00e9gions", "France")) %>%
    dplyr::filter(millesime == annee)

  # Étape 2 : Identifier la région sélectionnée
  zone_de_ref <- data %>%
    dplyr::mutate(dplyr::across(dplyr::where(is.factor), as.character)) %>%
    dplyr::filter(Zone_de_ref == TRUE) %>%
    dplyr::pull(Zone) %>%
    unique()

  # Étape 3 : Identifier le territoire français de comparaison
  zone_de_comp <- data %>%
    dplyr::mutate(dplyr::across(dplyr::where(is.factor), as.character)) %>%
    dplyr::filter(Zone == zone_de_ref) %>%
    dplyr::pull(Zone_de_comp) %>%
    unique()

  # Étape 4 : Utilisation des formulations idiomatiques
  id_reg <- data %>%
    dplyr::filter(Zone == zone_de_ref) %>%
    dplyr::pull(CodeZone) %>%
    unique() %>%
    as.character()
  verb_reg <- propre.datareg::datareg(code_reg = id_reg)

  # Étape 5 : Calcul des indicateurs de rang
  `%notin%` = Negate(`%in%`)

  rang <- data %>%
    dplyr::filter(Zone_de_comp == zone_de_comp) %>%
    dplyr::filter(CodeZone %notin% c("FRMETRO", "DROM")) %>%
    dplyr::arrange(cout_prev_maison_moy_m2) %>%
    dplyr::mutate(
      rang_moins_cher = round(rank(cout_prev_maison_moy_m2), 0),
      rang_plus_cher = round(max(rank(cout_prev_maison_moy_m2)) - rank(cout_prev_maison_moy_m2)) + 1
    ) %>%
    dplyr::mutate(
      rang_zone_de_ref = dplyr::case_when(
        rang_moins_cher < 8 ~ paste0("au ", rang_moins_cher,"e rang des r\u00e9gions les moins ch\u00e8res"),
        rang_plus_cher < 7 ~ paste0("au ", rang_plus_cher,"e rang des r\u00e9gions les plus ch\u00e8res"),
        TRUE ~ NA_character_ # Cas par défaut si aucune condition n'est satisfaite
      )
    )

  # Étape 6 : Création de la liste des paramètres
  verb1 <- list(
    # la_region = verb_reg$nom_region,
    # nom_region = verb_reg$nom_region ,
    cout_prev_maison_moy_m2_zone_de_ref = data %>%
      dplyr::filter(Zone == zone_de_ref) %>%
      dplyr::pull(cout_prev_maison_moy_m2) %>%
      mapfactory::format_fr_nb(dec = 0, big_mark = " "),
    rang_zone_de_ref = rang %>%
      dplyr::filter(Zone == zone_de_ref) %>%
      dplyr::pull(rang_zone_de_ref)
  )

  # Étape 7 : Générer le texte final
  phrase1 <- glue::glue(
    "En {annee}, avec un co\u00fbt de construction d\u2019une maison de {verb1$cout_prev_maison_moy_m2_zone_de_ref} \u20ac/m\u00b2, {verb_reg$la_region} se situe {verb1$rang_zone_de_ref}."
  )

  # Étape 8 : Retourner le résultat
  verbatim <- glue::glue("{phrase1}")

  test <- data %>%
    dplyr::filter(Zone == zone_de_ref) %>%
    dplyr::select(
      "cout_prev_maison_moy_m2"
    )
  test <- sum(is.na(test))

  if(test == 0){
    verbatim
  }else{
    id_verb =  "3_4"
    print(paste0("Attention : le verbatim ", id_verb, " contient des NA."))
    verbatim
  }
}
