#'Création du verbatim 3 du chapitre 4
#'
#' @description Production du verbatim du chapitre 4.
#'
#' @param data La table d'indicateurs préparée par dataprep() selon les inputs de l'utilisateur.
#' @param annee Le millesime renseigné par l'utilisateur.
#'
#' @return Une chaîne de caractères comprenant les commentaires essentiels du chapitre 3.
#'
#' @importFrom dplyr select filter mutate pull case_when left_join
#' @importFrom glue glue
#' @importFrom mapfactory format_fr_nb
#' @export
#'
#' @examples
#' indicateurs_eptb <- lire_eptb_exemple()
#' creer_verbatim_4_3(data = indicateurs_eptb, annee = 2023)
#'
creer_verbatim_4_3 <- function(data, annee) {

  # filtrer les données pour les régions
  data <- data %>%
    dplyr::select(
      "TypeZone": "Zone_de_comp",
      "part_projet_ca_1":"part_projet_ca_5",
      "cout_projet_moy_ca_1":"cout_projet_moy_ca_5"
    ) %>%
    dplyr::filter(TypeZone %in% c("R\u00e9gions")) %>%
    dplyr::filter(millesime == annee)

  # identifier la région sélectionnée
  zone_de_ref <- data %>%
    dplyr::mutate(across(where(is.factor), as.character)) %>%
    dplyr::filter(Zone_de_ref == TRUE) %>%
    dplyr::pull(Zone) %>%
    unique()

  data <- dplyr::filter(data, Zone == zone_de_ref)

  population_age_reg <- dplyr::filter(population_age, nom_reg == zone_de_ref)

  # tester la complétude du jeu de données
  test <- data %>%
    dplyr::select(
      "part_projet_ca_1",
      "part_projet_ca_2",
      "part_projet_ca_3",
      "part_projet_ca_4",
      "part_projet_ca_5"
    )
  test <- sum(is.na(test))

  if(test == 0){
    mou_max <- data %>%
      dplyr::mutate(
        menage_max_part_projet = dplyr::case_when(
          part_projet_ca_1 == pmax(
            part_projet_ca_1, part_projet_ca_2, part_projet_ca_3, part_projet_ca_4, part_projet_ca_5,
            na.rm = TRUE
            ) ~ "moins de 30 ans",
          part_projet_ca_2 == pmax(
            part_projet_ca_1, part_projet_ca_2, part_projet_ca_3, part_projet_ca_4, part_projet_ca_5,
            na.rm = TRUE
          ) ~ "30 \u00e0 39 ans",
          part_projet_ca_3 == pmax(
            part_projet_ca_1, part_projet_ca_2, part_projet_ca_3, part_projet_ca_4, part_projet_ca_5,
            na.rm = TRUE
          ) ~ "40 \u00e0 49 ans",
          part_projet_ca_4 == pmax(
            part_projet_ca_1, part_projet_ca_2, part_projet_ca_3, part_projet_ca_4, part_projet_ca_5,
            na.rm = TRUE
          ) ~ "50 \u00e0 59 ans",
          part_projet_ca_5 == pmax(
            part_projet_ca_1, part_projet_ca_2, part_projet_ca_3, part_projet_ca_4, part_projet_ca_5,
            na.rm = TRUE
          ) ~ "60 ans ou plus"
        )
      )
  }else{
    mou_max <- data %>%
      dplyr::mutate(
        menage_max_part_projet = NA
      )
  }

  cout_projet_moyen_ca <- mou_max %>%
    dplyr::mutate(
      cout_projet_moyen_ca = dplyr::case_when(
        menage_max_part_projet == "moins de 30 ans" ~ cout_projet_moy_ca_1,
        menage_max_part_projet == "30 \u00e0 39 ans" ~ cout_projet_moy_ca_2,
        menage_max_part_projet == "40 \u00e0 49 ans" ~ cout_projet_moy_ca_3,
        menage_max_part_projet == "50 \u00e0 59 ans" ~ cout_projet_moy_ca_4,
        menage_max_part_projet == "60 ans ou plus" ~ cout_projet_moy_ca_5,
        is.na(menage_max_part_projet) ~ NA
        )
      ) %>%
    dplyr::left_join(population_age_reg, by=c("Zone" = "nom_reg", "CodeZone" = "code_reg")) %>%
    dplyr::mutate(
      part_de_cette_tranche_dans_le_RP = dplyr::case_when(
        menage_max_part_projet == "moins de 30 ans" ~ part_15_a_29_ans,
        menage_max_part_projet == "30 \u00e0 39 ans" ~ part_30_a_39_ans,
        menage_max_part_projet == "40 \u00e0 49 ans" ~ part_40_a_49_ans,
        menage_max_part_projet == "50 \u00e0 59 ans" ~ part_50_a_59_ans,
        menage_max_part_projet == "60 ans ou plus" ~ part_60_ans_ou_plus,
        is.na(menage_max_part_projet) ~ NA
      )
    )

  indicateur <- cout_projet_moyen_ca %>%
    dplyr::mutate(
      part_max = max(
        part_projet_ca_1,
        part_projet_ca_2,
        part_projet_ca_3,
        part_projet_ca_4,
        part_projet_ca_5
      )
    ) %>%
    dplyr::select(
      "TypeZone": "Zone_de_comp",
      "menage_max_part_projet",
      "part_max",
      "part_de_cette_tranche_dans_le_RP",
      "cout_projet_moyen_ca"
    )

  # création d'une liste nommée des différents paramètres
  verb1 <- list(
    menage_max_part_projet_zone_de_ref = indicateur$menage_max_part_projet,
    part_max_zone_de_ref = indicateur$part_max,
    part_de_cette_tranche_dans_le_RP_zone_de_ref = indicateur$part_de_cette_tranche_dans_le_RP,
    cout_projet_moyen_ca_zone_de_ref = indicateur$cout_projet_moyen_ca %>%
      mapfactory::format_fr_nb(dec = 0, big_mark = " ")
  )

  # phrases
  phrase1 <- glue::glue("Les {verb1$menage_max_part_projet_zone_de_ref} repr\u00e9sentent la part la plus importante des m\u00e9nages qui font construire une maison.")

  phrase2 <- glue::glue("Ils correspondent \u00e0 {verb1$part_max_zone_de_ref} % des ma\u00eetres d\u2019ouvrage ")

  phrase3 <- glue::glue("et \u00e0 {verb1$part_de_cette_tranche_dans_le_RP_zone_de_ref} % de la population r\u00e9gionale.")

  phrase4 <- glue::glue("Le co\u00fbt total du projet pour cette classe d\u2019\u00e2ge s\u2019\u00e9l\u00e8ve \u00e0 {verb1$cout_projet_moyen_ca_zone_de_ref} \u20ac.")

  # retourner le résultat
  verbatim <- glue::glue("{phrase1}\n\n{phrase2}{phrase3}\n\n{phrase4}")

  if(test == 0){
    verbatim
  }else{
    id_verb =  "4_3"
    print(paste0("Attention : le verbatim ", id_verb, " contient des NA."))
    verbatim
  }

}
