#' Creation du tableau 1 du chapitre 4 (Projets de construction par tranche d’age des maitres d’ouvrage pour la region selectionnee)
#'
#' @description Mise en page du tableau du chapitre 4 au format html.
#'
#' @param data La table d'indicateurs préparée par dataprep() selon les inputs de l'utilisateur.
#' @param annee Le millesime renseigné par l'utilisateur.
#' @param titre une chaine de caractère si vous voulez ajouter un titre spécifique.
#' par défaut "Projets de construction par tranche d’âge des maîtres d’ouvrage"
#' @param note_de_lecture une chaine de caractère si vous voulez ajouter une note de lecture en dessous des sources. Par défaut vide.
#' @param add_scroll un booleen pour indique si l'on souhaite inserer une scrollbox. (par défaut FALSE)
#'
#' @return Un tableau mis en page au format html.
#'
#' @importFrom dplyr mutate_if filter select pull full_join arrange mutate case_when if_else
#' @importFrom kableExtra kable kable_styling row_spec footnote scroll_box
#' @importFrom tidyr contains pivot_longer
#' @export
#'
#' @examples
#' indicateurs_eptb <- lire_eptb_exemple()
#' creer_tableau_4_1(
#' data = indicateurs_eptb,
#' annee = 2023,
#' titre = "Projets de construction par tranche d\u2019\u00e2ge des ma\u00eetres d\u2019ouvrage",
#' add_scroll = FALSE,
#' note_de_lecture = ""
#' )
#'
creer_tableau_4_1 <- function(data, annee, titre, note_de_lecture, add_scroll){

  zone_de_ref <- data %>%
    dplyr::mutate_if(is.factor, as.character) %>%
    dplyr::filter(Zone_de_ref == TRUE) %>%
    dplyr::select("Zone") %>%
    dplyr::pull() %>%
    unique()

  id_reg <- dplyr::filter(data, TypeZone == "R\u00e9gions", Zone == zone_de_ref) %>%
    dplyr::pull("CodeZone") %>% unique %>% as.character

  ch4tab <- data %>%
    dplyr::select(
      "TypeZone",
      "Zone", "millesime",
      "part_projet_ca_1":"part_projet_ca_5",
      "cout_projet_moy_ca_1":"cout_projet_moy_ca_5",
      # "cout_projet_moy_tot"="cout_projet_moy",
      "part_prix_terrain_cout_projet_ca_1":"part_prix_terrain_cout_projet_ca_5",
      # "part_prix_terrain_cout_projet_tot"="part_prix_terrain_cout_projet",
      "part_residence_princ_ca_1":"part_residence_princ_ca_5",
      # "part_residence_princ_tot"="part_residence_princ"
    ) %>%
    dplyr::filter(TypeZone == "R\u00e9gions", Zone == zone_de_ref) %>%
    dplyr::filter(millesime == annee) %>%
    dplyr::mutate_if(is.factor, as.character) #%>%
    # dplyr::mutate(part_projet_tot = 100)

  ch4tab_part_projet <- ch4tab %>%
    dplyr::select("Zone", "millesime", tidyr::contains("part_projet")) %>%
    tidyr::pivot_longer(
      tidyr::contains("part_projet"),
      names_to = "tranche_age",
      names_prefix = "part_projet_",
      values_to = "part_projet"
      )

  ch4tab_cout_projet_moy <- ch4tab %>%
    dplyr::select( tidyr::contains("cout_projet_moy")    ) %>%
    tidyr::pivot_longer(
      tidyr::contains("cout_projet_moy"),
      names_to = "tranche_age",
      names_prefix = "cout_projet_moy_",
      values_to = "cout_projet_moy"
      )

  ch4tab_part_prix_terrain <- ch4tab %>%
    dplyr::select(tidyr::contains("part_prix_terrain_cout_projet")) %>%
    tidyr::pivot_longer(
      tidyr::contains("part_prix_terrain_cout_projet"),
      names_to = "tranche_age",
      names_prefix = "part_prix_terrain_cout_projet_",
      values_to = "part_prix_terrain_cout_projet"
      )

  ch4tab_part_residence_princ <- ch4tab %>%
    dplyr::select(tidyr::contains("part_residence_princ")) %>%
    tidyr::pivot_longer(
      tidyr::contains("part_residence_princ"),
      names_to = "tranche_age",
      names_prefix = "part_residence_princ_",
      values_to = "part_residence_princ"
      )

  ch4table_complet <- ch4tab_part_projet %>%
    dplyr::full_join(ch4tab_cout_projet_moy, by="tranche_age") %>%
    dplyr::full_join(ch4tab_part_prix_terrain, by="tranche_age") %>%
    dplyr::full_join(ch4tab_part_residence_princ, by="tranche_age") %>%
    dplyr::arrange(tranche_age) %>%
    dplyr::mutate(
      tranche_age = dplyr::case_when(
        tranche_age == "ca_1" ~ "Moins de 30 ans",
        tranche_age == "ca_2" ~ "30 \u00e0 39 ans",
        tranche_age == "ca_3" ~ "40 \u00e0 49 ans",
        tranche_age == "ca_4" ~ "50 \u00e0 59 ans",
        tranche_age == "ca_5" ~ "60 ans et plus",
        # tranche_age == "tot" ~ "Ensemble",
        TRUE ~ "erreur"
        )
      )

  ch4tableau <- ch4table_complet %>%
    dplyr::select(
      "tranche_age", "part_projet", "cout_projet_moy", "part_prix_terrain_cout_projet", "part_residence_princ"
    ) %>%
    # Mise en place des titres de colonnes
    kableExtra::kable(
      "html",
      col.names = c(
        "Tranches d\u2019\u00e2ge", "R\u00e9partition des projets (%)", "Prix moyen du projet (\u20ac)",
        "Part du terrain dans le co\u00fbt du projet (%)",
        "Part des constructions destin\u00e9es \u00e0 l\u2019habitation (%)"
      ),
      escape = FALSE,
      digits = c(0, 2, 2, 2, 2),
      format.args = list(big.mark = " ", decimal.mark = ","),
      caption = titre
    ) %>%
    kableExtra::kable_styling(font_size = 12) %>%
    # Formatage des lignes "Ensemble" : fond gris foncé, gras
    # kableExtra::row_spec(which(dplyr::pull(ch4table_complet, "tranche_age") == "Ensemble"), bold = TRUE, italic = FALSE, background = "#C4C4C4") %>%
    # Formatage des autres lignes
    kableExtra::row_spec(which(dplyr::pull(ch4table_complet, "tranche_age") != "Ensemble"), bold = FALSE, background = "#FFFFFF") %>%
    # Creation note de bas de page avec la source et l'ajout parametrable d une note de lecture
    kableExtra::footnote(
      general = paste0(
        dplyr::if_else(
          note_de_lecture != "",
          paste0(note_de_lecture, "\n\n", caption(millesime = annee)),
          caption(millesime = annee)
        ),
        general_title = ""
      )
    )

  if (add_scroll) {
    ch4tableau <- ch4tableau %>%
      kableExtra::scroll_box(width = "100%", height = "500px", fixed_thead = TRUE)
  }

  # donnees a faire figurer dans l'export xls
  data_xls <- ch4table_complet

  # metadonnees a faire figurer dans la table des matiere de l'export xls
  index <- data.frame(onglet = "tableau_4_1", titre = titre)

  return(list(viz = ch4tableau, tab_xls = data_xls, meta = index))

}

