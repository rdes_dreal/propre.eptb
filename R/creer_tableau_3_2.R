#' Creation du tableau 2 du chapitre 3
#'
#' @description Mise en page du tableau du chapitre 3 au format html.
#'
#' @param data La table d'indicateurs préparée par dataprep() selon les inputs de l'utilisateur.
#' @param annee Le millesime renseigné par l'utilisateur.
#' @param titre une chaine de caractère si vous voulez ajouter un titre spécifique.
#' @param note_de_lecture une chaine de caractère si vous voulez ajouter une note de lecture en dessous des sources
#' @param add_scroll un booleen pour indique si l'on souhaite inserer une scrollbox. (par défaut FALSE)
#'
#' @return Un tableau mis en page au format html.
#'
#' @importFrom dplyr select mutate_if filter pull mutate bind_rows if_else
#' @importFrom glue glue
#' @importFrom kableExtra kable kable_styling footnote scroll_box
#' @importFrom tidyselect everything
#' @export
#'
#' @examples
#' indicateurs_eptb <- lire_eptb_exemple()
#' creer_tableau_3_2(
#' data = indicateurs_eptb,
#' annee = 2023,
#' titre = "Caractéristiques des maisons par type de maître d'oeuvre",
#' add_scroll = FALSE,
#' note_de_lecture = "Un 's' remplace une valeur quand cette dernière à été masquée compte tenu du secret statistique et/ou de la qualité des données d'enquête."
#' )
#'
creer_tableau_3_2 <- function(data, annee, titre, note_de_lecture, add_scroll){

  data <- data %>%
    dplyr::select(
      "TypeZone":"Zone_de_comp",

      "part_moe_architecte",
      "part_moe_constructeur_maison_ind",
      "part_moe_entrepreneur_artisan",
      "part_moe_petitionnaire",
      "part_moe_autre",

      "surf_prev_maison_moy_moe_architecte",
      "surf_prev_maison_moy_moe_constructeur_maison_ind",
      "surf_prev_maison_moy_moe_entrepreneur_artisan",
      "surf_prev_maison_moy_moe_petitionnaire",
      "surf_prev_maison_moy_moe_autre",
      # "surf_prev_maison_moy",

      "cout_prev_maison_moy_moe_architecte",
      "cout_prev_maison_moy_moe_constructeur_maison_ind",
      "cout_prev_maison_moy_moe_entrepreneur_artisan",
      "cout_prev_maison_moy_moe_petitionnaire",
      "cout_prev_maison_moy_moe_autre",
      # "cout_prev_maison_moy",

      "part_maison_finition_1_moe_architecte",
      "part_maison_finition_1_moe_constructeur_maison_ind",
      "part_maison_finition_1_moe_entrepreneur_artisan",
      "part_maison_finition_1_moe_petitionnaire",
      "part_maison_finition_1_moe_autre"
      # "part_maison_finition_1"
      ) %>%
    dplyr::mutate(
      part_maison_finition_1_moe_architecte = round(part_maison_finition_1_moe_architecte, digits = 0),
      part_maison_finition_1_moe_constructeur_maison_ind = round(part_maison_finition_1_moe_constructeur_maison_ind, digits = 0),
      part_maison_finition_1_moe_entrepreneur_artisan = round(part_maison_finition_1_moe_entrepreneur_artisan, digits = 0),
      part_maison_finition_1_moe_petitionnaire = round(part_maison_finition_1_moe_petitionnaire, digits = 0),
      part_maison_finition_1_moe_autre = round(part_maison_finition_1_moe_autre, digits = 0)
      # part_maison_finition_1
    )

  zone_de_ref <- data %>%
    dplyr::mutate_if(is.factor, as.character) %>%
    dplyr::filter(Zone_de_ref == TRUE) %>%
    dplyr::select("Zone") %>%
    dplyr::pull() %>%
    unique()

  zone_de_comp <- data %>%
    dplyr::mutate_if(is.factor, as.character) %>%
    dplyr::filter(Zone == zone_de_ref) %>%
    dplyr::pull("Zone_de_comp") %>%
    unique()

  id_reg <- dplyr::filter(data, Zone == zone_de_ref) %>%
    dplyr::pull("CodeZone") %>% unique %>% as.character

  data <- data %>%
    dplyr::filter((TypeZone %in% c("R\u00e9gions")) & Zone == zone_de_ref) %>%
    dplyr::filter(millesime == annee) %>%
    dplyr::mutate_if(is.factor, as.character)

  data_architecte <- data %>%
    dplyr::select(
      "part_moe" = "part_moe_architecte",
      "surf_prev_maison_moy" = "surf_prev_maison_moy_moe_architecte",
      "cout_prev_maison_moy" = "cout_prev_maison_moy_moe_architecte",
      "part_maison_finition_1" = "part_maison_finition_1_moe_architecte"
      ) %>%
    dplyr::mutate(moe = "Architecte") %>%
    dplyr::select(moe, tidyselect::everything())

  data_constructeur_maison_ind <- data %>%
    dplyr::select(
      "part_moe" = "part_moe_constructeur_maison_ind",
      "surf_prev_maison_moy" = "surf_prev_maison_moy_moe_constructeur_maison_ind",
      "cout_prev_maison_moy" = "cout_prev_maison_moy_moe_constructeur_maison_ind",
      "part_maison_finition_1" = "part_maison_finition_1_moe_constructeur_maison_ind"
    ) %>%
    dplyr::mutate(moe = "Constructeur") %>%
    dplyr::select(moe, tidyselect::everything())

  data_entrepreneur_artisan <- data %>%
    dplyr::select(
      "part_moe" = "part_moe_entrepreneur_artisan",
      "surf_prev_maison_moy" = "surf_prev_maison_moy_moe_entrepreneur_artisan",
      "cout_prev_maison_moy" = "cout_prev_maison_moy_moe_entrepreneur_artisan",
      "part_maison_finition_1" = "part_maison_finition_1_moe_entrepreneur_artisan"
    ) %>%
    dplyr::mutate(moe = "Entrepreneur ou artisan") %>%
    dplyr::select(moe, tidyselect::everything())

  data_petitionnaire <- data %>%
    dplyr::select(
      "part_moe" = "part_moe_petitionnaire",
      "surf_prev_maison_moy" = "surf_prev_maison_moy_moe_petitionnaire",
      "cout_prev_maison_moy" = "cout_prev_maison_moy_moe_petitionnaire",
      "part_maison_finition_1" = "part_maison_finition_1_moe_petitionnaire"
    ) %>%
    dplyr::mutate(moe = "Particulier") %>%
    dplyr::select(moe, tidyselect::everything())

  data_autre <- data %>%
    dplyr::select(
      "part_moe" = "part_moe_autre",
      "surf_prev_maison_moy" = "surf_prev_maison_moy_moe_autre",
      "cout_prev_maison_moy" = "cout_prev_maison_moy_moe_autre",
      "part_maison_finition_1" = "part_maison_finition_1_moe_autre"
    ) %>%
    dplyr::mutate(moe = "Autre cas") %>%
    dplyr::select(moe, tidyselect::everything())

  # data_ensemble <- data %>%
  #   dplyr::select(
  #     "surf_prev_maison_moy",
  #     "cout_prev_maison_moy",
  #     "part_maison_finition_1"
  #   ) %>%
  #   dplyr::mutate(
  #     moe = "ensemble",
  #     part_moe = 100.00,
  #     ) %>%
  #   dplyr::select(moe, part_moe, tidyselect::everything())

  tab <- dplyr::bind_rows(
    data_architecte,
    data_constructeur_maison_ind,
    data_entrepreneur_artisan,
    data_petitionnaire,
    data_autre,
    # data_ensemble
  )

  tableau <- tab %>%
    # Mise en place des titres de colonnes
    kableExtra::kable(
      "html",
      col.names = c(
        "Type de ma\u00eetre d\'oeuvre",
        "R\u00e9partition (en %)",
        "Surface moyenne des maisons (en m\u00b2)",
        "Prix moyen (en \u20ac/m\u00b2)",
        "Part de maisons totalement termin\u00e9es (en %)"
      ),
      escape = FALSE,
      digits = c(0, 0, 2, 2, 2, 2, 2, 2),
      format.args = list(big.mark = " ", decimal.mark = ","),
      caption = glue::glue(titre)
      ) %>%
    kableExtra::kable_styling(font_size = 12) %>%
    kableExtra::footnote(
      general = paste0(
        dplyr::if_else(
          note_de_lecture != "",
          paste0(note_de_lecture, "\n\n", caption(millesime = annee)),
          caption(millesime = annee)
        ),
        general_title = ""
      )
    )

  if (add_scroll) {
    tableau <- tableau %>%
      kableExtra::scroll_box(width = "100%", height = "500px", fixed_thead = TRUE)
  }

  # donnees a faire figurer dans l'export xls
  data_xls <- tab

  # metadonnees a faire figurer dans la table des matiere de l'export xls
  index <- data.frame(onglet = "tableau_3_2", titre = glue::glue(titre))

  return(list(viz = tableau, tab_xls = data_xls, meta = index))

}
