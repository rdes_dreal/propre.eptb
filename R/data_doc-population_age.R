#' population_age
#'
#' Table de la population générale selon la classe d'âge du chef de ménage.
#'
#' @format A data frame with 21 rows and 7 variables:
#' \describe{
  #'   \item{ code_reg }{  character }
  #'   \item{ nom_reg }{  character }
  #'   \item{ part_15_a_29_ans }{  numeric }
  #'   \item{ part_30_a_39_ans }{  numeric }
  #'   \item{ part_40_a_49_ans }{  numeric }
  #'   \item{ part_50_a_59_ans }{  numeric }
  #'   \item{ part_60_ans_ou_plus }{  numeric }
  #' }
  #' @source DREAL Pays de la Loire
  "population_age"
