#' Creation du graphe 4 du chapitre 3 (histogramme du prix et surfaces moyens par mode de chauffage des maisons)
#'
#' @description Mise en forme du graphe du chapitre 3 au format html.
#'
#' @param data La table d'indicateurs préparée par dataprep() selon les inputs de l'utilisateur.
#' @param annee Le millesime renseigné par l'utilisateur.
#' @param note_de_lecture une chaine de caractère si vous voulez ajouter une note de lecture en dessous des sources
#' @param palette Un vecteur ou une palette de couleurs pour les barres du graphique.
#'
#' @return Un graphe intéractif mis en forme au format html.
#'
#' @importFrom dplyr filter select starts_with ends_with mutate_if pull mutate recode case_when arrange group_by summarize
#' @importFrom ggiraph geom_bar_interactive girafe
#' @importFrom ggplot2 ggplot aes position_dodge coord_flip labs margin guides guide_legend theme element_text scale_y_continuous sec_axis
#' @importFrom glue glue
#' @importFrom gouvdown theme_gouv scale_fill_gouv_discrete
#' @importFrom tidyr pivot_longer
#' @export
#'
#' @examples
#'  indicateurs_eptb <- lire_eptb_exemple()
#' creer_graphe_3_4(
#' data = indicateurs_eptb,
#' annee = 2023,
#' palette = "pal_gouv_qual2",
#' note_de_lecture = "")[["viz"]]
#'

creer_graphe_3_4 <- function(
    data = indicateurs_eptb,
    annee = annee,
    palette = "pal_gouv_qual2",
    note_de_lecture = ""
){
  data <- data %>%
    dplyr::filter(
      TypeZone %in% c("R\u00e9gions", "France"),
      millesime == annee
    ) %>%
    dplyr::select(
      "TypeZone":"Zone_de_comp",
      dplyr::starts_with("surf_prev_maison_moy_chauf_"),
      dplyr::starts_with("cout_moy_maison_chauff_")& dplyr::ends_with("m2")

    )

  zone_de_ref <- data %>%
    dplyr::mutate_if(is.factor, as.character) %>%
    dplyr::filter(Zone_de_ref == TRUE) %>%
    dplyr::select("Zone") %>%
    dplyr::pull() %>%
    unique()

  zone_de_comp <- data %>%
    dplyr::mutate_if(is.factor, as.character) %>%
    dplyr::filter(Zone == zone_de_ref) %>%
    dplyr::pull("Zone_de_comp") %>%
    unique()

  # Création de la table utile à la production du graphique
  tab <- data %>%
    #dplyr::filter(Zone %in% c(zone_de_ref, zone_de_comp)) %>%
    dplyr::mutate(TypeZone = factor(TypeZone, levels = c("R\u00e9gions", "France"))) %>%
    dplyr::filter(!(TypeZone %in% c("R\u00e9gions")) | Zone == zone_de_ref) %>%
    dplyr::filter(!(TypeZone %in% c("France")) | Zone == zone_de_comp) %>%
    dplyr::select(
      "TypeZone":"Zone","millesime",
      dplyr::starts_with("surf_prev_maison_moy_chauf_"),
      dplyr::starts_with("cout_moy_maison_chauff_")
      ) %>%
    tidyr::pivot_longer(
      cols = dplyr::starts_with("surf_prev_maison_moy_chauf_") | dplyr::starts_with("cout_moy_maison_chauff_"),
      names_to = c("type", "mode_chauffage"),
      names_pattern = "(surf_prev_maison_moy_chauf_|cout_moy_maison_chauff_)(\\d+)",
      values_to = "valeur"
      ) %>%
    # Créer une colonne pour différencier "surface" et "prix"
    dplyr::mutate(
      type = dplyr::recode(type, "surf_prev_maison_moy_chauf_" = "surface", "cout_moy_maison_chauff_" = "prix")
      )%>%
    dplyr::mutate(
      mode_chauffage = dplyr::case_when(
        mode_chauffage == 6 ~ "Autres modes de chauffage",
        mode_chauffage == 5 ~ "\u00c9nergies renouvelables combin\u00e9es \u00e0 un autre mode",
        mode_chauffage == 4 ~ "\u00c9nergies renouvelables seules ou combin\u00e9es entre elles",
        mode_chauffage == 3 ~ "\u00c9lectricit\u00e9 et bois d'appoint",
        mode_chauffage == 2 ~ "\u00c9lectricit\u00e9 seule",
        mode_chauffage == 1 ~ "Gaz"
        )
      ) %>%
    dplyr::filter(valeur != 0) %>%
    dplyr::arrange(millesime, mode_chauffage)

  titre = glue::glue("Prix et surfaces moyens par mode de chauffage des maisons en {annee} ")


  # Facteurs d'échelle (par exemple, ajuster selon vos données)
  surface_ajuste <- tab %>%
    dplyr::group_by(type) %>%
    dplyr::summarize(max_val = max(valeur, na.rm = TRUE)) %>%
    dplyr::filter(type == "surface") %>%
    dplyr::pull(max_val) /
    tab %>%
    dplyr::group_by(type) %>%
    dplyr::summarize(max_val = max(valeur, na.rm = TRUE)) %>%
    dplyr::filter(type == "prix") %>%
    dplyr::pull(max_val)

  # Ajuster les données pour correspondre à l'échelle
  tab <- tab %>%
    dplyr::mutate(
      valeur_ajuste = dplyr::case_when(
        type == "prix" ~ valeur,
        type == "surface" ~ valeur / surface_ajuste
      )
    )

  g <- ggplot2::ggplot(
    data = tab,
    mapping = ggplot2::aes(
      x = .data$mode_chauffage,
      y = .data$valeur_ajuste,
      fill = .data$type)
    ) +
    ggiraph::geom_bar_interactive(
      stat = "identity", data_id = row.names(tab),
      tooltip = dplyr::pull(tab, "valeur"),
      position = ggplot2::position_dodge()) +
    ggplot2::coord_flip() +
    ggplot2::labs(
      title = titre,
      x = "",
      y = ""
    ) +
    gouvdown::theme_gouv(
      base_size = 18,
      plot_title_size = 30,
      subtitle_size  = 28,
      plot_title_face = "plain",
      axis_text_size = 18,
      plot_margin = ggplot2::margin(5, 5, 5, 5)
    )+
    ggplot2::guides(fill = ggplot2::guide_legend(title = "")) +
    ggplot2::theme(
      legend.position = "bottom",
      axis.title.y = ggplot2::element_text(hjust = 0.5),  # Centrer le titre principal de l'axe y
      axis.title.y.right = ggplot2::element_text(hjust = 0.5),  # Centrer le titre de l'axe secondaire
      axis.title.x = ggplot2::element_text(hjust = 0.5)  # Centrer le titre de l'axe x
      )+
    ggplot2::scale_y_continuous(
      name = "Prix (\u20ac/m\u00b2)",
      sec.axis = ggplot2::sec_axis(
        trans = ~ . * surface_ajuste,
        name = "Surface (m\u00b2)"
        )
      )+
      gouvdown::scale_fill_gouv_discrete(palette = palette)


  graphe <- ggiraph::girafe(
    code = print(g),
    width_svg = 16,  # Largeur en pouces
    height_svg = 9   # Hauteur en pouces
    )

  # donnees a faire figurer dans l'export xls
  data_xls <- tab %>%
    dplyr::select(-c("TypeZone","valeur_ajuste"))

  # metadonnees a faire figurer dans la table des matiere de l'export xls
  index <- data.frame(onglet = "graphe_3_4", titre = glue::glue(titre) %>% gsub("\n", " ", .))

  return(list(viz = graphe, tab_xls = data_xls, meta = index))

}



