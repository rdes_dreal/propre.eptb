#'Création du verbatim 9 du chapitre 3
#'
#' @description Production du verbatim du chapitre 3.
#'
#' @param data La table d'indicateurs préparée par dataprep() selon les inputs de l'utilisateur.
#' @param annee Le millesime renseigné par l'utilisateur.
#'
#' @return Une chaîne de caractères comprenant les commentaires essentiels du chapitre 3.
#'
#' @importFrom dplyr select filter mutate pull contains everything bind_rows left_join slice bind_cols
#' @importFrom glue glue
#' @importFrom tidyr pivot_longer
#' @export
#'
#' @examples
#' indicateurs_eptb <- lire_eptb_exemple()
#' creer_verbatim_3_9(data = indicateurs_eptb, annee = 2023)
#'
creer_verbatim_3_9 <- function(data, annee) {

  # un dataframe qui contient tous les libellés de moe
  type_moe <- data.frame(
    var = c("architecte", "constructeur_maison_ind", "entrepreneur_artisan", "petitionnaire", "autre", NA_character_),
    lib = c("un architecte", "un constructeur de maisons individuelles", "un entrepreneur ou un artisan", "le petitionnaire", "une autre personne", NA_character_)
  )

  # filtrer les données pour les régions
  data <- data %>%
    dplyr::select(
      "TypeZone": "Zone_de_comp",
      "part_maison_finition_1",
      "cout_prev_maison_moyen_finition_1_m2",
      "part_maison_finition_1_moe_architecte",
      "part_maison_finition_1_moe_constructeur_maison_ind",
      "part_maison_finition_1_moe_entrepreneur_artisan",
      "part_maison_finition_1_moe_petitionnaire",
      "part_maison_finition_1_moe_autre"
    ) %>%
    dplyr::filter(.data$TypeZone %in% c("R\u00e9gions")) %>%
    dplyr::filter(.data$millesime == annee)

  # identifier la région sélectionnée
  zone_de_ref <- data %>%
    dplyr::mutate(across(where(is.factor), as.character)) %>%
    dplyr::filter(.data$Zone_de_ref == TRUE) %>%
    dplyr::pull("Zone") %>%
    unique()

  data <- dplyr::filter(data, Zone == zone_de_ref)

  moe_min <- data %>%
    dplyr::select(dplyr::contains("part_maison_finition_1_")) %>%
    tidyr::pivot_longer(cols = dplyr::everything(), names_to = "type_moe", values_to = "part") %>%
    dplyr::filter(part == min(part, na.rm = FALSE)) %>%
    # on ajoute une 2e ligne tout à NA au cas où le résultat du filtre serait vide
    dplyr::bind_rows(c(type_moe = NA, part = NA)) %>%
    # on récupère le libelle du moe dans les données saisies plus haut
    dplyr::mutate(type_moe = gsub("part_maison_finition_1_moe_", "", type_moe)) %>%
    dplyr::left_join(type_moe, by = c("type_moe" = "var")) %>%
    # on ne garde que la premiere ligne qui contient les valeurs si elles existent ou les NA
    dplyr::slice(1) %>%
    dplyr::select(moe_min = "lib", part_min = "part")


  indicateur <- data %>%
    dplyr::bind_cols(moe_min) %>%
    dplyr::select(
      "TypeZone", "Zone_de_comp",
      "part_maison_finition_1",
      "cout_prev_maison_moyen_finition_1_m2",
      "part_min",
      "moe_min"
    )

  # création d'une liste nommée des différents paramètres
  verb1 <- list(
    part_maison_finition_1_zone_de_ref = indicateur$part_maison_finition_1,
    cout_prev_maison_moyen_finition_1_m2_zone_de_ref = indicateur$cout_prev_maison_moyen_finition_1_m2,
    part_min_zone_de_ref = indicateur$part_min,
    moe_min_zone_de_ref = indicateur$moe_min
    )

  # detection de NA dans les composants du verbatim
  message_si_na(composants_verbatim = verb1, id_verb = "3_9")

  # phrases
  phrase1 <-"Ce prix moyen d\u00e9pend \u00e9galement du degr\u00e9 de finition pr\u00e9vu pour la construction."

  phrase2 <- glue::glue(" En moyenne, {verb1$part_maison_finition_1_zone_de_ref} % des maisons seront livr\u00e9es enti\u00e8rement achev\u00e9es, pour un co\u00fbt moyen d\u00e9clar\u00e9 d\u2019environ {verb1$cout_prev_maison_moyen_finition_1_m2_zone_de_ref} \u20ac/m\u00b2.")

  phrase3 <- glue::glue("Ce taux est le plus bas, soit {verb1$part_min_zone_de_ref} %, dans le cas o\u00f9 o\u00f9 le ma\u00eetre d\u2019\u0153uvre est {verb1$moe_min_zone_de_ref}.")

  # retourner le résultat
  verbatim <- glue::glue("{phrase1}{phrase2}\n\n{phrase3}")

  return(verbatim)

}
