#'Création du verbatim 7 du chapitre 3
#'
#' @description Production du verbatim du chapitre 3.
#'
#' @param data La table d'indicateurs préparée par dataprep() selon les inputs de l'utilisateur.
#' @param annee Le millesime renseigné par l'utilisateur.
#'
#' @return Une chaîne de caractères comprenant les commentaires essentiels du chapitre 3.
#'
#' @importFrom dplyr select filter mutate pull left_join
#' @importFrom glue glue
#' @export
#'
#' @examples
#' indicateurs_eptb <- lire_eptb_exemple()
#' creer_verbatim_3_7(data = indicateurs_eptb, annee = 2023)
#'
creer_verbatim_3_7 <- function(data, annee) {

  # filtrer les données pour les régions
  data <- data %>%
    dplyr::select(
      "TypeZone": "Zone_de_comp",
      "surf_prev_maison_moy_moe_architecte",
      "surf_prev_maison_moy_moe_constructeur_maison_ind",
      "surf_prev_maison_moy_moe_entrepreneur_artisan",
      "surf_prev_maison_moy_moe_petitionnaire",
      "surf_prev_maison_moy_moe_autre"
    ) %>%
    dplyr::filter(TypeZone %in% c("R\u00e9gions")) %>%
    dplyr::filter(millesime == annee)

  # identifier la région sélectionnée
  zone_de_ref <- data %>%
    dplyr::mutate(across(where(is.factor), as.character)) %>%
    dplyr::filter(Zone_de_ref == TRUE) %>%
    dplyr::pull(Zone) %>%
    unique()

  data <- dplyr::filter(data, Zone == zone_de_ref)

  # calcul des indicateurs MOE max et MOE min
  moe_max <- data %>%
    dplyr::mutate(moe_max_var = case_when(
      surf_prev_maison_moy_moe_architecte == pmax(
        surf_prev_maison_moy_moe_architecte,
        surf_prev_maison_moy_moe_constructeur_maison_ind,
        surf_prev_maison_moy_moe_entrepreneur_artisan,
        surf_prev_maison_moy_moe_petitionnaire,
        surf_prev_maison_moy_moe_autre,
        na.rm = TRUE) ~ "un architecte",

      surf_prev_maison_moy_moe_constructeur_maison_ind == pmax(
        surf_prev_maison_moy_moe_architecte,
        surf_prev_maison_moy_moe_constructeur_maison_ind,
        surf_prev_maison_moy_moe_entrepreneur_artisan,
        surf_prev_maison_moy_moe_petitionnaire,
        surf_prev_maison_moy_moe_autre,
        na.rm = TRUE) ~ "un constructeur de maison individuelle",

      surf_prev_maison_moy_moe_entrepreneur_artisan == pmax(
        surf_prev_maison_moy_moe_architecte,
        surf_prev_maison_moy_moe_constructeur_maison_ind,
        surf_prev_maison_moy_moe_entrepreneur_artisan,
        surf_prev_maison_moy_moe_petitionnaire,
        surf_prev_maison_moy_moe_autre,
        na.rm = TRUE) ~ "un entrepreneur ou un artisan",

      surf_prev_maison_moy_moe_petitionnaire == pmax(
        surf_prev_maison_moy_moe_architecte,
        surf_prev_maison_moy_moe_constructeur_maison_ind,
        surf_prev_maison_moy_moe_entrepreneur_artisan,
        surf_prev_maison_moy_moe_petitionnaire,
        surf_prev_maison_moy_moe_autre,
        na.rm = TRUE) ~ "le petitionnaire",

      surf_prev_maison_moy_moe_autre == pmax(
        surf_prev_maison_moy_moe_architecte,
        surf_prev_maison_moy_moe_constructeur_maison_ind,
        surf_prev_maison_moy_moe_entrepreneur_artisan,
        surf_prev_maison_moy_moe_petitionnaire,
        surf_prev_maison_moy_moe_autre,
        na.rm = TRUE) ~ "une autre personne"
    )
    ) %>%
    dplyr::select("TypeZone": "Zone_de_comp", moe_max_var)

  moe_min <- data %>%
    dplyr::mutate(moe_min_var = case_when(
      surf_prev_maison_moy_moe_architecte == pmin(
        surf_prev_maison_moy_moe_architecte,
        surf_prev_maison_moy_moe_constructeur_maison_ind,
        surf_prev_maison_moy_moe_entrepreneur_artisan,
        surf_prev_maison_moy_moe_petitionnaire,
        surf_prev_maison_moy_moe_autre,
        na.rm = TRUE) ~ "un architecte",

      surf_prev_maison_moy_moe_constructeur_maison_ind == pmin(
        surf_prev_maison_moy_moe_architecte,
        surf_prev_maison_moy_moe_constructeur_maison_ind,
        surf_prev_maison_moy_moe_entrepreneur_artisan,
        surf_prev_maison_moy_moe_petitionnaire,
        surf_prev_maison_moy_moe_autre,
        na.rm = TRUE) ~ "un constructeur de maison individuelle",

      surf_prev_maison_moy_moe_entrepreneur_artisan == pmin(
        surf_prev_maison_moy_moe_architecte,
        surf_prev_maison_moy_moe_constructeur_maison_ind,
        surf_prev_maison_moy_moe_entrepreneur_artisan,
        surf_prev_maison_moy_moe_petitionnaire,
        surf_prev_maison_moy_moe_autre,
        na.rm = TRUE) ~ "un entrepreneur ou un artisan",

      surf_prev_maison_moy_moe_petitionnaire == pmin (
        surf_prev_maison_moy_moe_architecte,
        surf_prev_maison_moy_moe_constructeur_maison_ind,
        surf_prev_maison_moy_moe_entrepreneur_artisan,
        surf_prev_maison_moy_moe_petitionnaire,
        surf_prev_maison_moy_moe_autre,
        na.rm = TRUE) ~ "le petitionnaire",

      surf_prev_maison_moy_moe_autre == pmin(
        surf_prev_maison_moy_moe_architecte,
        surf_prev_maison_moy_moe_constructeur_maison_ind,
        surf_prev_maison_moy_moe_entrepreneur_artisan,
        surf_prev_maison_moy_moe_petitionnaire,
        surf_prev_maison_moy_moe_autre,
        na.rm = TRUE) ~ "une autre personne"
    )
    ) %>%
    dplyr::select("TypeZone": "Zone_de_comp", moe_min_var)

  indicateur <- dplyr::left_join(moe_max, moe_min)

  # création d'une liste nommée des différents paramètres
  verb1 <- list(
    moe_max_zone_de_ref = indicateur$moe_max_var ,
    moe_min_zone_de_ref = indicateur$moe_min_var)

  # générer le texte final
  phrase1 <- glue::glue(
    "C\u2019est quand le ma\u00eetre d\u2019\u0153uvre est {verb1$moe_max_zone_de_ref} que la surface moyenne de la maison est la plus haute. Elle est la plus basse dans le cas d\u2019{verb1$moe_min_zone_de_ref}."
  )

  # retourner le résultat
  verbatim <- glue::glue("{phrase1}")

  test <- data %>%
    dplyr::select(
      "surf_prev_maison_moy_moe_architecte",
      "surf_prev_maison_moy_moe_constructeur_maison_ind",
      "surf_prev_maison_moy_moe_entrepreneur_artisan",
      "surf_prev_maison_moy_moe_petitionnaire",
      "surf_prev_maison_moy_moe_autre"
    )
  test <- sum(is.na(test))

  if(test == 0){
    verbatim
  }else{
    id_verb =  "3_7"
    print(paste0("Attention : le verbatim ", id_verb, " contient des NA."))
    verbatim
  }

}
