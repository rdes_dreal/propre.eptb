#' population_active
#'
#' Table de la populations active en nombre et en part selon la csp du chef de ménage.
#'
#' @format A data frame with 19 rows and 17 variables:
#' \describe{
  #'   \item{ code_reg }{  character }
  #'   \item{ nom_reg }{  character }
  #'   \item{ part_csp_1 }{  numeric }
  #'   \item{ part_csp_2 }{  numeric }
  #'   \item{ part_csp_3 }{  numeric }
  #'   \item{ part_csp_4 }{  numeric }
  #'   \item{ part_csp_5 }{  numeric }
  #'   \item{ part_csp_6 }{  numeric }
  #'   \item{ part_csp_autre }{  numeric }
  #'   \item{ pop_active }{  numeric }
  #'   \item{ pop_csp_1 }{  numeric }
  #'   \item{ pop_csp_2 }{  numeric }
  #'   \item{ pop_csp_3 }{  numeric }
  #'   \item{ pop_csp_4 }{  numeric }
  #'   \item{ pop_csp_5 }{  numeric }
  #'   \item{ pop_csp_6 }{  numeric }
  #'   \item{ pop_csp_autre }{  numeric }
  #' }
  #' @source DREAL Pays de la Loire
  "population_active"
