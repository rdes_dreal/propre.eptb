#'Création du verbatim 13 du chapitre 3
#'
#' @description Production du verbatim du chapitre 3.
#'
#' @param data La table d'indicateurs préparée par dataprep() selon les inputs de l'utilisateur.
#' @param annee Le millesime renseigné par l'utilisateur.
#'
#' @return Une chaîne de caractères comprenant les commentaires essentiels du chapitre 3.
#'
#' @importFrom dplyr select filter mutate pull
#' @importFrom glue glue
#' @importFrom propre.datareg datareg
#' @export
#'
#' @examples
#' indicateurs_eptb <- lire_eptb_exemple()
#' creer_verbatim_3_13(data = indicateurs_eptb, annee = 2023 )
#'
creer_verbatim_3_13 <- function(data, annee) {

  # Étape 1 : Filtrer les données pour les régions de métropole et la France
  data <- data %>%
    dplyr::select(
      "TypeZone": "Zone_de_comp",
      "surf_prev_maison_moy","part_projet_chauff_4","part_projet_chauffage_enr_et_associe",
      "surf_prev_maison_moy_chauf_4_cout_prev_maison","cout_moy_maison_chauff_4_m2"
    ) %>%
    dplyr::filter(TypeZone %in% c("R\u00e9gions", "France")) %>%
    dplyr::filter(millesime == annee)


  # Étape 2 : Identifier la région sélectionnée
  zone_de_ref <- data %>%
    dplyr::mutate(across(where(is.factor), as.character)) %>%
    dplyr::filter(Zone_de_ref == TRUE) %>%
    dplyr::pull(Zone) %>%
    unique()

  # Étape 3 : Identifier le territoire français de comparaison
  zone_de_comp <- data %>%
    dplyr::mutate(across(where(is.factor), as.character)) %>%
    dplyr::filter(Zone == zone_de_ref) %>%
    dplyr::pull(Zone_de_comp) %>%
    unique()

  # Étape 4 : indicateurs necessaires
  data <- data %>%
    dplyr::filter(Zone_de_comp == zone_de_comp | is.na(Zone_de_comp)) %>%
    dplyr::filter(millesime == annee)

  # Étape 5 :les formulations idiomatiques grâce à {propre.datareg}
  id_reg <- dplyr::filter(data, Zone == zone_de_ref) %>%
    dplyr::pull("CodeZone") %>% unique %>% as.character
  verb_reg <- propre.datareg::datareg(code_reg = id_reg)

  # Étape 6 : création d'une liste nommée des différents paramètres
  verb1 <- list(
    la_region = verb_reg$la_region,
    nom_region = verb_reg$nom_region ,
    part_projet_chauff_4_zone_de_ref = dplyr::filter(data, Zone == zone_de_ref) %>%
      dplyr::pull("part_projet_chauff_4") ,
    part_projet_chauff_4_zone_de_com = dplyr::filter(data, Zone == zone_de_comp) %>%
      dplyr::pull("part_projet_chauff_4") ,
    part_projet_chauffage_enr_et_associe_zone_de_ref = dplyr::filter(data, Zone == zone_de_ref) %>%
      dplyr::pull("part_projet_chauffage_enr_et_associe") ,
    part_projet_chauffage_enr_et_associe_zone_de_comp = dplyr::filter(data, Zone == zone_de_comp) %>%
      dplyr::pull("part_projet_chauffage_enr_et_associe") ,
    cout_moy_maison_chauff_4_m2_zone_de_ref = dplyr::filter(data, Zone == zone_de_ref) %>%
      dplyr::pull("cout_moy_maison_chauff_4_m2")  %>%
      format(big.mark = " ", scientific = FALSE),
    surf_prev_maison_moy_chauf_4_cout_prev_maison_zone_de_ref = dplyr::filter(data, Zone == zone_de_ref) %>%
      dplyr::pull("surf_prev_maison_moy_chauf_4_cout_prev_maison") ,
    surf_prev_maison_moy_zone_de_ref = dplyr::filter(data, Zone == zone_de_ref) %>%
      dplyr::pull("surf_prev_maison_moy")
  )


  # Étape 7 : première phrase
  phrase1 <- glue::glue(
    "Les \u00e9nergies renouvelables (bois, pompe \u00e0 chaleur, solaire thermique), seules ou combin\u00e9es entre elles ont \u00e9t\u00e9 choisies dans {verb1$part_projet_chauff_4_zone_de_ref} % des projets de construction de maison individuelle en {annee}."
  )

  phrase2 <- glue::glue(
    " Elles repr\u00e9sentent {verb1$part_projet_chauff_4_zone_de_com} % en {zone_de_comp}."
  )

  phrase3 <- glue::glue(
    " En prenant en compte les cas o\u00f9 elles sont associ\u00e9es \u00e0 un autre mode de chauffage, elles sont pr\u00e9sentes dans {verb1$part_projet_chauffage_enr_et_associe_zone_de_ref} % des projets r\u00e9gionaux et {verb1$part_projet_chauffage_enr_et_associe_zone_de_comp} % des projets en {zone_de_comp}."
  )

  phrase4 <- glue::glue(
    " Dans {verb1$la_region} {verb1$nom_region}, le prix moyen des maisons chauff\u00e9es aux \u00e9nergies renouvelables (seules ou combin\u00e9es entre elles) est de {verb1$cout_moy_maison_chauff_4_m2_zone_de_ref} \u20ac/m\u00b2. Leur surface est en moyenne de {verb1$surf_prev_maison_moy_chauf_4_cout_prev_maison_zone_de_ref} m\u00b2 \u00e0 comparer \u00e0 {verb1$surf_prev_maison_moy_zone_de_ref} m\u00b2 pour l\u2019ensemble des modes de chauffage."
  )

  verbatim <- if ("part_projet_chauff_4" > 50) {
    glue::glue("{phrase1}{phrase2}{phrase3}{phrase4}")
  } else {
    glue::glue("{phrase1}{phrase2}{phrase3}")
  }

  test <- data %>%
    dplyr::select(
      "surf_prev_maison_moy",
      "part_projet_chauff_4",
      "part_projet_chauffage_enr_et_associe",
      "surf_prev_maison_moy_chauf_4_cout_prev_maison",
      "cout_moy_maison_chauff_4_m2"
    )
  test <- sum(is.na(test))

  if(test == 0){
    verbatim
  }else{
    id_verb =  "3_13"
    print(paste0("Attention : le verbatim ", id_verb, " contient des NA."))
    verbatim
  }
}
