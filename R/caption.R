#' Mentionne la source dans les illustrations.
#'
#' @param millesime Une annee parmi les millesimes selectionnables par l'utilisateur, au format numerique.
#'
#' @return Une chaine de texte
#' @export
#'
#' @examples
#' caption(millesime = 2023)
#'

caption <- function(millesime = 2020){

  lib_eptb <- paste0("SDES, EPTB en ", millesime)

  lib <- paste0("Source : ", lib_eptb)

  return(lib)
}

