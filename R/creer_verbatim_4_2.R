#' Création du verbatim 2 du chapitre 4
#'
#' @description Production du verbatim 2 du chapitre 4.
#'
#' @param data La table d'indicateurs préparée par dataprep() selon les inputs de l'utilisateur.
#' @param annee Le millesime renseigné par l'utilisateur.
#'
#' @return Une chaîne de caractères comprenant les commentaires essentiels du chapitre 4.
#'
#' @importFrom dplyr mutate_if filter select pull
#' @importFrom glue glue
#' @importFrom mapfactory format_fr_nb
#' @importFrom propre.datareg datareg
#' @export
#'
#' @examples
#' indicateurs_eptb <- lire_eptb_exemple()
#' creer_verbatim_4_2(data = indicateurs_eptb, annee = 2023)
#'
creer_verbatim_4_2 <- function(data, annee){

  # on rappelle la région sélectionnée
  zone_de_ref <- data %>%
    dplyr::mutate_if(is.factor, as.character) %>%
    dplyr::filter(TypeZone == "R\u00e9gions", Zone_de_ref == TRUE) %>%
    dplyr::select("Zone") %>%
    dplyr::pull() %>%
    unique()

  # les formulations idiomatiques grâce à {propre.datareg}
  id_reg <- dplyr::filter(data, TypeZone == "R\u00e9gions", Zone == zone_de_ref) %>%
    dplyr::pull("CodeZone") %>% unique %>% as.character

  verb_reg <- propre.datareg::datareg(code_reg = id_reg)

  # on sélectionne la région, ainsi que le millésime
  data <- data %>%
    dplyr::filter(
      TypeZone == "R\u00e9gions",
      Zone == zone_de_ref,
      millesime == annee
    ) %>%
    dplyr::select(
      "TypeZone":"Zone_de_comp",
      "evolution_cout_prev_maison_moy_1an",
      "contribution_terrain_projet_evol_1an"
    )

  # on crée ensuite une liste nommee des differents parametres
  verb1 <- list(
    evolution_cout_prev_maison_moy_1an_zone_de_ref = data %>%
      dplyr::pull("evolution_cout_prev_maison_moy_1an") %>%
      mapfactory::format_fr_nb(dec = 0, big_mark = " "),
    contribution_terrain_projet_evol_1an_zone_de_ref = data %>%
      dplyr::pull("contribution_terrain_projet_evol_1an") %>%
      mapfactory::format_fr_nb(dec = 0, big_mark = " ")
  )

  phrase1 <- glue::glue(
    "Sur un an, le co\u00fbt total pr\u00e9visionnel d\'un projet de construction d\'une maison \u00e9volue de
    {verb1$evolution_cout_prev_maison_moy_1an_zone_de_ref} \u0025. "
  )

  phrase2 <- glue::glue(
    "Le prix des terrains y contribue \u00e0 hauteur de {verb1$contribution_terrain_projet_evol_1an_zone_de_ref} points."
  )

  verbatim <- glue::glue("{phrase1}{phrase2}")

  test <- data %>%
    dplyr::filter(Zone == zone_de_ref) %>%
    dplyr::select(
      "evolution_cout_prev_maison_moy_1an",
      "contribution_terrain_projet_evol_1an"
    )
  test <- sum(is.na(test))

  if(test == 0){
    verbatim
  }else{
    id_verb =  "4_2"
    print(paste0("Attention : le verbatim ", id_verb, " contient des NA."))
    verbatim
  }
}
