Etape de création de votre projet de publication :

 * Complétez les champs auteurs et date dans index.Rmd ;  

 * Complétez éventuellement la liste des epci de référence sur lesquels vous voulez un zoom dans les illustrations ;  

 * Lancez la compilation du bookdown ;  

 * Intégrez vos commentaires et analyses ;-)
