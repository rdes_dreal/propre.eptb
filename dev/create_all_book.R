library(tidyverse)
library(propre.eptb)
library(COGiter)
# remotes::install_github("pachevalier/tricky")


# Création des fichiers du book d'une région avec les données et le .Rproj --------
mil <- 2023
dir_export <- paste0("/nfs/data/partage-EPTB/propre.EPTB/book_auto_", mil, "/", Sys.Date(), "/")
dir.create(dir_export, recursive = TRUE, showWarnings = FALSE)

create_publi_reg <- function(reg = "Pays de la Loire", path = dir_export) {

  # nommage des projets et dossiers
  nom_projet <- paste0("eptb_", mapfactory::get_id_reg(reg), "_", tricky::str_standardize(reg))
  dir_export_reg <- paste0(path, nom_projet, "/")

  # Création du projet bookdown

  propre.eptb_skeleton(
    path = dir_export_reg,
    annee = mil,
    nom_region = paste0(mapfactory::get_id_reg(reg), " ", reg)
  )

  nom_rproj <- paste0(dir_export_reg, "/", nom_projet, ".Rproj")
  file.create(nom_rproj)
  contenu_rproj <- "Version: 1.0\n\nRestoreWorkspace: Default\nSaveWorkspace: Default\nAlwaysSaveHistory: Default\n\nEnableCodeIndexing: Yes\nUseSpacesForTab: Yes\nNumSpacesForTab: 2\nEncoding: UTF-8\n\nRnwWeave: Sweave\nLaTeX: pdfLaTeX\n\nAutoAppendNewline: Yes\nStripTrailingWhitespace: Yes\n\nBuildType: Website"

  write_lines(x = contenu_rproj, file = nom_rproj)
  print(paste0("Le book de la région ", reg, " est créé."))

}

# Création des projets régionaux avec les données et le .Rproj
map(.x = COGiter::regions$NOM_REG, .f = create_publi_reg)

# Fonction de compilation d'un book sans création du pdf------
render_publi_reg <- function(reg = "Pays de la Loire") {
  nom_projet <- paste0("eptb_", mapfactory::get_id_reg(reg), "_", tricky::str_standardize(reg))
  dir_export_reg <- paste0(dir_export, nom_projet)
  rmarkdown::render_site(input = dir_export_reg, encoding = 'UTF-8', )
  # creer_pdf_book(chemin_book = paste0(dir_export_reg, "/_book"),
  # nom_pdf = paste0(nom_projet, ".pdf"))
}

# Création des book régionaux HTML et PDF
map(COGiter::regions$NOM_REG, .f = render_publi_reg)






